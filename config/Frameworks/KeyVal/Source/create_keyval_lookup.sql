
DROP TABLE keyval_lookup;

DROP SEQUENCE keyval_sequence;

Create sequence keyval_sequence start with 1
increment by 1
minvalue 1
maxvalue 10000;

create table keyval_lookup ( 
    keyval_id number(10),
    cat varchar2(60),
    key varchar2(60),
    val varchar2(60),
    constraint pk_keyval_id PRIMARY KEY(keyval_id)
);

//unique constraint on Cat / att
//index on cat / att

/* EMEA */

insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|AT','AGS.supportdesk@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|BE','ELC.IT.Support@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|CH','AGS.supportdesk@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|CZ','CE.IThelpdesk@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|DE','AGS.supportdesk@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|DK','Nordic.SupportDesk@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|ES','Technology.Iberia@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|FI','Nordic.SupportDesk@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|FR','Hotline.France@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|GB','UK.Supportdesk@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|GR','Greece.IT.Helpdesk@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|HR','CE.IThelpdesk@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|HU','CE.IThelpdesk@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|IL','Technology.Israel@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|IT','Italy.Supportdesk@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|NL','EHQ.IT.HelpDesk@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|NO','Nordic.SupportDesk@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|PL','Daniel.Deniszewski@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|PT','Technology.Iberia@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|RU','Russia.SupportDesk@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|SE','Nordic.SupportDesk@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|SI','CE.IThelpdesk@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|SK','CE.IThelpdesk@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','EMEA|TR','SupportDesk.Turkey@nike.com');

/* Asia */

insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|AU','TIS.Nike.Pacific@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Converse Footwear Tech Se','ITsupport.ConverseChina@converse.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Converse footwear Technical|Shanghai','Converse.onboard@converse.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Converse Inc.|Shanghai','Converse.onboard@converse.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Converse Inc|Shanghai','Converse.onboard@converse.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Converse Sporting Goods','ITsupport.ConverseChina@converse.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Converse, Inc.|Shanghai','Converse.onboard@converse.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Converse, Inc|Shanghai','Converse.onboard@converse.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Converse|Shanghai','Converse.onboard@converse.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Nike Commercial (China)','ITPERFORMANCE.BAR@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Nike Inc|FUZHOU CITY','IT.Guangzhou@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Nike Inc|GUANGZHOU CITY','IT.Guangzhou@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Nike Inc|Guangzhou','IT.Guangzhou@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|NIKE Sourcing Guangzhou C','IT.Guangzhou@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Nike Sports (China) Co.','ITPERFORMANCE.BAR@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Nike, Inc.','ITPERFORMANCE.BAR@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Nike, Inc.|Beijing','ITPERFORMANCE.BAR@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Nike, Inc.|FUZHOU CITY','IT.Guangzhou@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Nike, Inc.|GUANGZHOU CITY','IT.Guangzhou@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Nike, Inc.|Guangzhou','IT.Guangzhou@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Nike, Inc.|SHANGDONG','IT.QINGDAO@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Nike, Inc.|Shanghai','SHLO.Onboard@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Nike|FUZHOU CITY','IT.Guangzhou@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Nike|GUANGZHOU CITY','IT.Guangzhou@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|CN|Nike|Guangzhou','IT.Guangzhou@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|HK','HongKong.ITSharedServices@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|ID','Indonesia.Systems@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|ID|PT Nike Indonesia','ITSupport.Indonesia@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|IN','NikeTechnology.India@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Hurley International|Fukuoka','TIS.HurleyJapan@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Hurley International|Osaka','TIS.HurleyJapan@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Hurley International|Shibuya','TIS.HurleyJapan@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Hurley International|Shinagawa','TIS.HurleyJapan@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Hurley International|Tomisato','TIS.HurleyJapan@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Hurley Japan HQ','TIS.HurleyJapan@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Chiba-ken','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Hachioji','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Koriyama','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Koto-ku','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Merguro-ku','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Minato-ku','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Nagano','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Okayama-Ken','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Osaka','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Pusan','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|S. Benedetto del Tronto','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Sapporo','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Shanghai','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Shibuya','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Shinagawa','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Taichung','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Tokyo','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Tomisato','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike (UK) Ltd|Yokohama','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|NIKE Japan Corporation','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike Japan Group LLC','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Chiba-ken','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Hachioji','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Koriyama','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Koto-ku','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Merguro-ku','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Minato-ku','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Nagano','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Okayama-Ken','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Osaka','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Pusan','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|S. Benedetto del Tronto','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Sapporo','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Shanghai','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Shibuya','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Shinagawa','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Taichung','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Tokyo','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Tomisato','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. / Nippon Express|Yokohama','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Chiba-ken','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Hachioji','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Koriyama','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Koto-ku','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Merguro-ku','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Minato-ku','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Nagano','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Okayama-Ken','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Osaka','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Pusan','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|S. Benedetto del Tronto','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Sapporo','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Shanghai','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Shibuya','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Shinagawa','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Taichung','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Tokyo','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Tomisato','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc. /Nippon Express|Yokohama','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Chiba-ken','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Hachioji','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Koriyama','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Koto-ku','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Merguro-ku','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Minato-ku','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Nagano','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Okayama-Ken','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Osaka','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Pusan','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|S. Benedetto del Tronto','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Sapporo','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Shanghai','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Shibuya','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Shinagawa','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Taichung','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Tokyo','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Tomisato','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc./ Nippon Express|Yokohama','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Chiba-ken','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Hachioji','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Koriyama','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Koto-ku','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Merguro-ku','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Minato-ku','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Nagano','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Okayama-Ken','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Osaka','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Pusan','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|S. Benedetto del Tronto','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Sapporo','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Shanghai','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Shibuya','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Shinagawa','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Taichung','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Tokyo','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Tomisato','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|JP|Nike, Inc.|Yokohama','Japan.TechOps.WFS@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|KR','TIS.NIKE.Korea@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|KR|Nike 360 (Korea)','TIS.NIKE.Korea@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|LK','NikeTechnology.India@Nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|MY','ITSupport.Malaysia@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|NZ','TIS.Nike.Pacific@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|PH','ITSupport.Philippines@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|SG','ITSS.Singapore@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|TH','ITSupport.Thailand@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|TW','Taiwan.TIS@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|TW|BRS Nike Taiwan, Inc.','Taiwan.TIS@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|TW|Nike 360 (Taiwan)','IT.Taichung@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|TW|Nike Taiwan Limited','Taiwan.TIS@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|TW|Nike, Inc.|Taichung','IT.Taichung@nike.com');
insert into keyval_lookup (keyval_id,cat,key,val) values(keyval_sequence.nextval,'JoinerEmails','Asia|VN','System.vietnam@nike.com');

commit;