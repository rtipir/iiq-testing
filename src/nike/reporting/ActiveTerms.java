package nike.reporting;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import sailpoint.api.SailPointContext;
import sailpoint.api.IdentityService;
import sailpoint.object.Attributes;
import sailpoint.object.Filter;
import sailpoint.object.Identity;
import sailpoint.object.LiveReport;
import sailpoint.object.QueryOptions;
import sailpoint.object.Sort;
import sailpoint.object.Application;
import sailpoint.object.Link;
import sailpoint.reporting.datasource.JavaDataSource;
import sailpoint.task.Monitor;
import sailpoint.tools.GeneralException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

/**
 * Custom Report - Terminated Users with Active Application Accounts.
 * Checks IIQ for Status flag T or 2 and gives applications which are still enabled/Active
 * 
 * @param  arguments
 * @return object
 *    
 *   Written By - Roshan Tipirneni 
 *   02/28/17
 */

public class ActiveTerms implements JavaDataSource {
	private Monitor monitor;
	private SailPointContext context;
	private QueryOptions queryForTermsWithApp;
	private QueryOptions queryForTerms;
	private Integer startRow;
	private Integer pageSize;
	private Object[] currentRow;
	private List<String> identityIds;
	private Iterator<Object[]> iterator;
	Iterator<Identity> termedAccounts;
	
	public void initialize(SailPointContext context, LiveReport report, Attributes<String, Object> arguments, String groupBy, List<Sort> sort) throws GeneralException {
		this.context = context;
		setMonitor(monitor);
		queryForTerms = new QueryOptions();
		identityIds = new ArrayList<String>();
		
		identityIds.add("Dummy Value");
		if (arguments.containsKey("applications")){
			List<String> applicationIds = arguments.getList("applications");
			for (String applinkids : applicationIds) {
				queryForTermsWithApp = new QueryOptions();
				List<String> applist = new ArrayList<String>();
				applist.add(applinkids);
				Filter hasAPP = Filter.in("links.application.id", applist);
				List<String> idStatus = new ArrayList();
				idStatus.add("Active");
		        idStatus.add("0");
		        idStatus.add("1");
		        Filter filter = Filter.or(Filter.not(Filter.in("status", idStatus)),Filter.isnull("status"));
				queryForTermsWithApp.addFilter(hasAPP);
				queryForTermsWithApp.addFilter(filter);
				termedAccounts = context.search(Identity.class, queryForTermsWithApp);
				IdentityService is = new IdentityService(context);
				while (termedAccounts.hasNext()) {
					Identity tempIdentity = termedAccounts.next();
					List<Link> tempIdentityLinks = tempIdentity.getLinks();
					for(int index=0; index<tempIdentityLinks.size();index++)
					 {	Link l =(Link)tempIdentityLinks.get(index);
						String appid = l.getApplicationId();
						if (appid.equals(applinkids) && !l.isDisabled())
						{
						identityIds.add(tempIdentity.getId());
						}
					 }
				}				
			}
		}
		
		if (!identityIds.isEmpty() && identityIds != null) {
			queryForTerms.add(Filter.in("id", identityIds));
		}

		if (sort != null) {
			for (Sort sortItem : sort) {
				queryForTerms.addOrdering(sortItem.getField(), sortItem.isAscending());
			}
		}

		if (groupBy != null) {
			queryForTerms.setGroupBys(Arrays.asList(groupBy));
		}
	}

	private void prepare() throws GeneralException {
		QueryOptions queryOptions = new QueryOptions(queryForTerms);
		
		if (startRow != null && startRow > 0) {
			queryOptions.setFirstRow(startRow);
		}
		if (pageSize != null && pageSize > 0) {
			queryOptions.setResultLimit(pageSize);
		}
		
		List<String> returnAttributes = Arrays.asList("employeeid", "displayName", "samaccountname", "status", "manager.displayName","email","name","isRetailAth","hireDate","termDate","accountExpires","leaverDate");
		iterator = context.search(Identity.class, queryOptions, returnAttributes);
	}

	public boolean next() throws JRException {
		if (iterator == null) {
			try {
				prepare();
			} catch (GeneralException e) {
				throw new JRException(e);
			}
		}
		if (iterator.hasNext()) {
			currentRow = iterator.next();
			return true;
		}
		return false;
	}

	public Object getFieldValue(String field) throws GeneralException {
		if ("employeeid".equals(field)) {
			if (currentRow[0] == null) return "BLANK";
			return currentRow[0];
		} else if ("displayName".equals(field)) {
			if (currentRow[1] == null) return "BLANK";
			return currentRow[1];
		} else if ("samaccountname".equals(field)) {
			if (currentRow[2] == null) return "BLANK";
			return currentRow[2];
		} else if ("status".equals(field)) {
			if (currentRow[3] == null) return "BLANK";
			return currentRow[3];
		} else if ("manager".equals(field)) {
			if (currentRow[4] == null) return "BLANK";
			return currentRow[4];
		} else if ("email".equals(field)) {
			if (currentRow[5] == null) return "BLANK";
			return currentRow[5];
		} else if ("name".equals(field)) {
			if (currentRow[6] == null) return "BLANK";
			return currentRow[6];
		} else if ("isRetailAth".equals(field)) {
			if (currentRow[7] == null) return "BLANK";
			return currentRow[7];
		} else if ("hireDate".equals(field)) {
			if (currentRow[8] == null) return "BLANK";
			return currentRow[8];
		} else if ("termDate".equals(field)) {
			if (currentRow[9] == null) return "BLANK";
			return currentRow[9];
		} else if ("accountExpires".equals(field)) {
			if (currentRow[10] == null) return "BLANK";
			return currentRow[10];
		} else if ("leaverDate".equals(field)) {
			if (currentRow[11] == null) return "BLANK";
			return currentRow[11];
		} else {
			throw new GeneralException("Unknown column '" + field + "'");
		}
	}

	public void setLimit(int startRow, int pageSize) {
		this.startRow = startRow;
		this.pageSize = pageSize;
	}

	public int getSizeEstimate() throws GeneralException {
		return context.countObjects(Identity.class, queryForTerms);
	}

	public void close() {
	}

	public Object getFieldValue(JRField jrField) throws JRException {
		String name = jrField.getName();
		try {
			return getFieldValue(name);
		} catch (GeneralException e) {
			throw new JRException(e);
		}
	}

	public void setMonitor(Monitor monitor) {
		this.monitor = monitor;
	}

	public QueryOptions getBaseQueryOptions() {
		return queryForTerms;
	}

	public String getBaseHql() {
		return null;
	}
}