package openconnector.connector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

import openconnector.AbstractConnector;
import openconnector.Connector;
import openconnector.ConnectorException;
import openconnector.Filter;
import openconnector.ConnectorConfig;
import openconnector.ObjectNotFoundException;
import openconnector.Result;

import murex.sdk.user.admin.Group;
import murex.sdk.user.admin.GroupCategory;
import murex.sdk.user.admin.LicenseCategory;
import murex.sdk.user.admin.User;
import murex.sdk.user.admin.UserAdminService;
import murex.sdk.user.admin.UserAdminServiceFactory;
import murex.sdk.user.admin.LicenseAdminService;
import murex.sdk.user.admin.LicenseAdminServiceFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author kerry.ebert@sailpoint.com 
 * 
 *         This connector is used to connect to Murex. 
 *         The first version will aggregate users, their groups,
 *         and disable the user and remove the user's license and groups.
 * 
 */
public class MurexConnector extends AbstractConnector {

	
	private static Log logger = LogFactory.getLog(MurexConnector.class);

    ////////////////////////////////////////////////////////////////////////////
    //
    // CONNECTIVITY ATTRIBUTE NAMES
    //
    ////////////////////////////////////////////////////////////////////////////

    /** The System Property key to set the Murex Application Codebase */
    private static final String MUREX_APPLICATION_CODEBASE_PROPERTY = "murex.application.codebase";
    private static final String MUREX_APPLICATION_CODEBASE_KEY = "codebase";
    
    /** Credentials of a user administrator (must belong to MX USER ADMINISTRATOR group) */
    private static final String USER_NAME_KEY = "username";
    private static final String USER_PASSWORD_KEY = "password";

    /** Determines whether password is encrypted. Use the monit tool to encrypt password */
    private static final String IS_PASSWORD_ENCRYPTED_KEY = "isEncrypted";
 
    /** The key to the Mx Site Name*/
    private static final String MX_SITE_NAME_KEY = "site";

    /** Used to delimit the token used to check status */
    private static final String TOKEN_DELIMETER = "|";
    
    ////////////////////////////////////////////////////////////////////////////
    //
    // SCHEMA ATTRIBUTE NAMES
    //
    ////////////////////////////////////////////////////////////////////////////

    //Shared schema attributes
    public static final String ATTR_NAME = "name";
    public static final String ATTR_UID = "uid";

    //Account Schema Attributes
    public static final String ATTR_ACCT_CODE = "code";
    public static final String ATTR_ACCT_DESCRIPTION = "description";
    public static final String ATTR_ACCT_SHORT_LABEL = "shortLabel";
    public static final String ATTR_ACCT_IS_DELETED = "isDeleted";
    public static final String ATTR_ACCT_IS_EXTERNAL = "isExternal";
    public static final String ATTR_ACCT_IS_LOCKED = "isLocked";
    public static final String ATTR_ACCT_IS_PUBLIC_PASS = "isPublicPassword";
    public static final String ATTR_ACCT_IS_SUSPENDED = "isSuspended";
    public static final String ATTR_ACCT_LICENSE_CATEGORY = "licenseCategory";
    public static final String ATTR_ACCT_GROUPS = "groups";
    
    //Group Schema Attributes
    public static final String ATTR_GRP_CATEGORY = "category";
    
    
    //~ ----------------------------------------------------------------------------------------------------------------
    //~ Instance fields 
    //~ ----------------------------------------------------------------------------------------------------------------

    /** The access point to the User Administration functions */
    private UserAdminService userAdminService;
	private LicenseAdminService licenseAdminService;

	
	public MurexConnector() {
		super();
	}
	 
	public MurexConnector(ConnectorConfig config, openconnector.Log log) {
		super(config, log);
		Properties props = System.getProperties();
		if (props.getProperty(MUREX_APPLICATION_CODEBASE_PROPERTY) == null)
			props.setProperty(MUREX_APPLICATION_CODEBASE_PROPERTY, config.getString(MUREX_APPLICATION_CODEBASE_KEY));
	}
	
    /*
     * Returns the supported features by the connector(non-Javadoc)
     * @see openconnector.AbstractConnector#getSupportedFeatures(java.lang.String)
     */
    public List<Feature> getSupportedFeatures(String objectType) {
        return Arrays.asList(Feature.values());
    }

    /*
     * Returns the type of supported objects(non-Javadoc)
     * @see openconnector.AbstractConnector#getSupportedObjectTypes()
     */
    public List<String> getSupportedObjectTypes() {
        List<String> types = super.getSupportedObjectTypes();
        types.add(OBJECT_TYPE_GROUP);
        types.add(OBJECT_TYPE_ACCOUNT);
        return types;
    }

	/**
	 * An iterator that holds all Tenrox user information
	 */
	private class MurexIterator implements Iterator<Map<String, Object>> {
		
		private Iterator<Map<String, Object>> murexIterator;
		Map<String, Object> obj;

		public MurexIterator(Iterator<Map<String, Object>> it) {
			this.murexIterator = it;
		}

		public boolean hasNext() {

			if (murexIterator.hasNext() == false) {
				return false;
			} else {
				return murexIterator.hasNext();
			}
		}

		public Map<String, Object> next() {

			obj = murexIterator.next();
			if (obj != null)
				remove();
			return obj;
		}

		public void remove() {
			murexIterator.remove();
		}
	}

	public void testConnection() throws openconnector.ConnectorException {
		logger.debug("Starting testConnection()...");
		//Hashtable h = RmiLoader.getMxProperties();
		//logger.debug(h.toString());
		try {
			if (userAdminService == null || licenseAdminService == null) {
				setUpServices();    
			}

			if (userAdminService == null) {
				throw new Exception("Unable to create new userAdminService object");
			}
			if (licenseAdminService == null) {
				throw new Exception("Unable to create new licenseAdminService object");
			}
			tearDown();

		} catch (Exception e) {
			if (logger.isErrorEnabled())
				logger.error("Unable to connect to MUREX system "
						+ e.getMessage());
			throw new openconnector.ConnectorException(e);
		}
		logger.debug("Ending testConnection().");
	}

    /**
     * <p>Set up fresh connection to the {@link UserAdminService} and {@link LincenseAdminService}</p>
     *
     * @throws Exception - when the service creation fails
     */
	public void setUpServices() throws Exception {
    	
		logger.debug("Starting setUpServices()...");
		
		if (config == null)
			throw new openconnector.ConnectorException("Config object empty");

		logger.debug("getting setting from config object: " + config.toString());
		String siteName = config.getString(MX_SITE_NAME_KEY);
		String codebaseURL = config.getString(MUREX_APPLICATION_CODEBASE_KEY);
		String username = config.getString(USER_NAME_KEY);
		String password = config.getString(USER_PASSWORD_KEY);
		boolean isEncrypted = config.getBoolean(IS_PASSWORD_ENCRYPTED_KEY);
		
		System.setProperty(MUREX_APPLICATION_CODEBASE_PROPERTY, codebaseURL);
		
		logger.debug("System property " + MUREX_APPLICATION_CODEBASE_PROPERTY + ": " + System.getProperty(MUREX_APPLICATION_CODEBASE_PROPERTY));
    	
		/* MX middleware arguments (must contain at least <tt>MXJ_SITE_NAME</tt>) */
		String[] MXJ_ARGS = { "/MXJ_SITE_NAME:" + siteName };
		logger.info("Connecting to user admin service (Murex Server: " + codebaseURL + ", middleware args: " + Arrays.asList(MXJ_ARGS) + ") ...");
		/* Use the UserAdminServiceFactory and LicenseAdminServiceFactory to retrieve a connection to the service */
		if (userAdminService == null)
			userAdminService = UserAdminServiceFactory.getUserAdminService(username, password, isEncrypted, MXJ_ARGS);
    	if (licenseAdminService == null)
    		licenseAdminService = LicenseAdminServiceFactory.getLicenseAdminService(username, password, isEncrypted, MXJ_ARGS);
    	
    	if (userAdminService == null || licenseAdminService == null) {
    		logger.error("could not create instance of user or license admin service");
    		throw new openconnector.ConnectorException("Could not connect to Murex");    		
    	}
    	logger.debug("Ending setUpServices().");
    }

	public Iterator<Map<String, Object>> iterate(Filter filter)
			throws ConnectorException, UnsupportedOperationException {

		logger.debug("Starting iterate()...");
		openconnector.Schema schema = null;
		List<Map<String, Object>> murexObjectList = new ArrayList<Map<String, Object>>();

		try {
			logger.debug("About to setup services.");
			setUpServices();
			
			if (OBJECT_TYPE_ACCOUNT.equals(getObjectType())) {
				schema = config.getSchema(Connector.OBJECT_TYPE_ACCOUNT);
				User[] users = userAdminService.getUsers();
				if (users != null) {
					if (logger.isDebugEnabled()) {
						logger.debug("Fetched [" + users.length + "] number of accounts from Murex");
					}
					for (User user : users) {
						Map<String, Object> murexUserMap = convertUserObjectToMap(user, schema);
						if (!isEmpty(murexUserMap))
							murexObjectList.add(murexUserMap);
					}
				}
				else {
					logger.debug("Did not return any users.");
				}
			}
			else if (OBJECT_TYPE_GROUP.equals(getObjectType())) {
				schema = config.getSchema(Connector.OBJECT_TYPE_GROUP);
				// TODO: Fix this API!
				Group[] groups = userAdminService.getGroups(null);
				if (groups != null) {
					if (logger.isDebugEnabled()) {
						logger.debug("Fetched [" + groups.length + "] number of groups from Murex");
					}
					for (Group group : groups) {
						Map<String, Object> murexGroupMap = convertGroupObjectToMap(group, schema);
						if (!isEmpty(murexGroupMap))
							murexObjectList.add(murexGroupMap);
					}
				}
				else {
					logger.debug("Did not return any groups.");
				}
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled())
				logger.error("Exception occured during " + getObjectType()
						+ " aggregation" + e.getMessage());
			throw new openconnector.ConnectorException(e);
		}
		logger.debug("About to tearDown().");
		tearDown();
		logger.debug("Ending iterate().");
		return new MurexIterator(murexObjectList.iterator());
	}

	public Map<String, Object> read(String id)
			throws openconnector.ConnectorException, ObjectNotFoundException,
			UnsupportedOperationException {
		
		logger.debug("Starting read()...");
		openconnector.Schema schema = null;
		Map<String, Object> _returnMap = null;

		try {
			setUpServices();

			if (OBJECT_TYPE_ACCOUNT.equals(getObjectType())) {
				schema = config.getSchema(Connector.OBJECT_TYPE_ACCOUNT);
				User user = getUserById(id);
				if (user != null) {
					_returnMap = convertUserObjectToMap(user, schema);
				}
			}
			if (OBJECT_TYPE_GROUP.equals(getObjectType())) {
				schema = config.getSchema(Connector.OBJECT_TYPE_GROUP);
				Group group = getGroupById(id);
				if (group != null) {
					_returnMap = convertGroupObjectToMap(group, schema);
				}
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled())
				logger.error("Error occurred during fetch of Murex object with ID[ "
						+ id + "]" + e.getMessage());
			throw new openconnector.ConnectorException(e);
		}

		if (_returnMap == null) {
			String msg = "Cannot fetch Murex object with ID[ " + id + "]";
			if (logger.isErrorEnabled())
				logger.error(msg);
			throw new ObjectNotFoundException(id);
		}
		logger.debug("About to tearDown().");
		tearDown();
		logger.debug("Ending read().");
		return _returnMap;
	}
	
	public Result enable(String id, Map<String, Object> options)
			throws ConnectorException, ObjectNotFoundException,
			UnsupportedOperationException {
		logger.debug("Starting enable()...");
		return updateSuspendStatus(id, options, false);
	}

	public Result disable(String id, Map<String, Object> options)
			throws ConnectorException, ObjectNotFoundException,
			UnsupportedOperationException {
		logger.debug("Starting disable()...");
		return updateSuspendStatus(id, options, true);
	}

	public Result unlock(String id, Map<String, Object> options)
			throws ConnectorException, ObjectNotFoundException,
			UnsupportedOperationException {
		logger.debug("Starting unlock()...");
		Result result = new Result(Result.Status.Committed);
		
		try {
			logger.debug("Setting up services.");
			setUpServices();
       
			logger.debug("Retrieving user with id = " + id);
			User user = getUserById(id);
			if (user != null && user.isLocked()) {				
				logger.info("Unlocking user '" + id + "' ...");
				
				// ***NOTE: this simply won't work until we get an unlock API***
				// TODO: figure out when this API will work
				//user.unlock();
				
				userAdminService.updateUser(user);
			}
			else {
				logger.error("User not found or is already unlocked with id = " + id);
				tearDown();
				return new Result(Result.Status.Failed);
			}

			/* Re-request the user and check the unlock status */
			User updatedUser = getUserById(id);
			if (updatedUser != null) {
				if (updatedUser.isLocked()) {
					logger.error("Cannot unlock user with id = " + id);
					tearDown();
					return new Result(Result.Status.Failed);
				}
				Map<String, Object> _returnMap = null;
				_returnMap = convertUserObjectToMap(updatedUser, schema);
				if (_returnMap != null)
					result.setObject(_returnMap);
			}
			tearDown();
		} catch (Exception e) {
			if (logger.isErrorEnabled())
				logger.error("Error occurred when unlocking Murex user with ID[ " 
						+ id + "]" + e.getMessage());
			if (shouldRetry(e)) {
				return new Result(Result.Status.Retry);
			}
			tearDown();
			throw new openconnector.ConnectorException(e);
		}
		logger.debug("Ending unlock().");
		return result;
	}
	
	private Result updateSuspendStatus(String id, Map<String, Object> options, boolean setSuspend)
			throws ConnectorException, ObjectNotFoundException,
			UnsupportedOperationException {
		logger.debug("Starting updateSuspendStatus()...");

		// TODO: 4-eyes
		// if 4-eye approvals are removed and the API worked synchronously,
		// then we will default the status to committed
		// Result result = new Result(Result.Status.Committed);
		// Setting default status to Queued so that the changes can be verified by 
		Result result = new Result(Result.Status.Committed);
		String operation = "";
		if (setSuspend) {
			operation = "Disable";
		}
		else {
			operation = "Enable";
		}
		
		// NOT currently used -- see the checkStatus() method comments for more information
		// setting token id as "Disable|81" -- will be used to check to see if the user is enabled/disabled appropriately
		// this will be a lot harder if we ever try to support update operations
		result.setToken(operation + TOKEN_DELIMETER + id);
		
		try {
			logger.debug("Setting up services.");
			setUpServices();
       
			logger.debug("Retrieving user with id = " + id);
			User user = getUserById(id);
			if (user != null) {				
				logger.info((setSuspend ? "Suspending" : "Unsuspending") + " user '" + id + "' ...");
				/* Commit the changes by using the userAdminService to update the user */
				user.setSuspended(setSuspend);
		        /* Get the license category, if any, that the user belongs to */
				if (setSuspend) {
					LicenseCategory licenseCategory = getLicenseCategory(user.getName());
			        /* If the License Category exists, release */
			        if (licenseCategory != null) {
			    		logger.info("Removing license " + licenseCategory.getName() + " from user '" + id + "' ...");
			            licenseAdminService.removeUserFromLicenseCategory(user, licenseCategory);
			            licenseAdminService.save();
			        }
			        else {
			        	logger.info("User '" + id + "' has no license to remove.");
			        }
				}
				// unsubscribe the user from all groups
				Group[] groups = userAdminService.getUserGroups(user);
				if (groups != null && groups.length > 0) {
					logger.info("Unsubscribing user '" + id + "' from groups...");
		            
					for (Group group : groups) {
						logger.info("Unsubscribing user '" + id + "' from group " + group.getName());
						userAdminService.unsubscribeUserFromGroup(user, group);
					}
				}
				else {
					logger.info("User '" + id + "' has no groups from which to unsubscribe.");
				}				
				
				logger.debug("Saving changes to user '" + id + "' ...");				
				userAdminService.updateUser(user);
			}
			else {
				logger.error("User not found with id = " + id);
				return new Result(Result.Status.Failed);
			}

			/* Re-request the user and check the suspension status */
			User updatedUser = getUserById(id);
			if (updatedUser != null) {
				
				if (updatedUser.isSuspended() != setSuspend) {
					// This is probably an indication that 4-eyes approvals are enabled
					// since we failed to suspend the user
					
					// changing status to queued so that we can verify that the suspension happens in the future
					result.setStatus(Result.Status.Queued);
				}
				
				Map<String, Object> _returnMap = null;
				_returnMap = convertUserObjectToMap(updatedUser, schema);
				if (_returnMap != null)
					result.setObject(_returnMap);
			}
			tearDown();
		} catch (Exception e) {
			if (logger.isErrorEnabled())
				logger.error("Error occurred when setting user suspend value to " 
						+ setSuspend + " for Murex user with ID[ " 
						+ id + "]" + e.getMessage());
			if (shouldRetry(e)) {
				return new Result(Result.Status.Retry);
			}
			tearDown();
			throw new openconnector.ConnectorException(e);
		}
		logger.debug("Ending updateSuspendStatus() : " + result.toJson());
		
		return result;
	}
	

	/*
	 * This method will call getters of User object and populate required Map.
	 * 
	 */
	private Map<String, Object> convertUserObjectToMap(User user, openconnector.Schema schema) {
		logger.debug("Starting convertUserObjectToMap()...");
		Map<String, Object> _returnMap = new HashMap<String, Object>();
		List<openconnector.Schema.Attribute> attrs = schema.getAttributes();
		if (isEmpty(attrs))
			throw new ConnectorException("Cannot retrieve user schema attributes for Murex connector.");

		try {
			for (openconnector.Schema.Attribute att : attrs) {
				if (att != null) {
					String attName = att.getName();
					Object attVal = null;
					switch (attName) {
		                case ATTR_NAME: 	
		                	attVal = user.getName();
		                	break;
		                case ATTR_UID: 
		                	attVal = String.valueOf(user.getUID());
		                	break;
		                case ATTR_ACCT_CODE:
		                	attVal = user.getCode();
		                	break;
		                case ATTR_ACCT_DESCRIPTION:
		                	attVal = user.getDescription();
		                	break;
		                case ATTR_ACCT_IS_DELETED:
		                	attVal = user.isDeleted();
		                	break;
		                case ATTR_ACCT_IS_EXTERNAL:
		                	attVal = user.isExternal();
		                	break;
		                case ATTR_ACCT_IS_LOCKED:
		                	attVal = user.isLocked();
		                	break;
		                case ATTR_ACCT_IS_PUBLIC_PASS:
		                	attVal = user.isPublicPassword();
		                	break;
		                case ATTR_ACCT_IS_SUSPENDED:
		                	attVal = user.isSuspended();
		                	if (attVal != null && (boolean) attVal) {
		                		_returnMap.put(Connector.ATT_DISABLED, true);
		                	}
		                	else {
		                		_returnMap.put(Connector.ATT_DISABLED, false);
		                	}
		                	break;
		                case ATTR_ACCT_SHORT_LABEL:
		                	attVal = user.getShortLabel();
		                	break;
		                case ATTR_ACCT_LICENSE_CATEGORY:
		                	LicenseCategory lc = getLicenseCategory(user.getName());
		                	if (lc != null) {
		                		attVal = lc.getName();
		                	}
		                	break;
		                case ATTR_ACCT_GROUPS:
		                	Group[] groups = userAdminService.getUserGroups(user);
		                	List groupNames = new ArrayList();
		                	if (groups != null) {
		                        for (Group group : groups) {
		                        	groupNames.add(group.getName());
		                        }	                		
		                	}
		                	attVal = groupNames;
		                	break;
		                default:
		                	logger.error("Unknown attribute name " + attName);
		                	break;
					}
					if (attVal != null)
						_returnMap.put(attName, attVal);
				}
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Exception occured while traversing User list "
						+ e.getMessage());
			}
		}
		logger.debug("Ending convertUserObjectToMap(): " + _returnMap.toString());
		return _returnMap;

	}
	
	/*
	 * This method will call getters of User object and populate required Map.
	 * 
	 */
	private Map<String, Object> convertGroupObjectToMap(Group group, openconnector.Schema schema) {
		logger.debug("Starting convertGroupObjectToMap()...");
		Map<String, Object> _returnMap = new HashMap<String, Object>();
		List<openconnector.Schema.Attribute> attrs = schema.getAttributes();
		if (isEmpty(attrs))
			throw new ConnectorException("Cannot retrieve group schema attributes for Murex connector.");

		try {
			for (openconnector.Schema.Attribute att : attrs) {
				if (att != null) {
					String attName = att.getName();
					Object attVal = null;
					switch (attName) {
		                case ATTR_NAME: 	
		                	attVal = group.getName();
		                	break;
		                case ATTR_UID: 
		                	attVal = String.valueOf(group.getUID());
		                	break;
		                case ATTR_GRP_CATEGORY:
		                	attVal = group.getCategory().toString();
		                	break;
		                default:
		                	logger.error("Unknown attribute name " + attName);
		                	break;
					}
					if (attVal != null)
						_returnMap.put(attName, attVal);
				}	
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Exception occured while traversing Group list "
						+ e.getMessage());
			}
		}
		logger.debug("Ending convertGroupObjectToMap(): " + _returnMap.toString());
		return _returnMap;

	}

	// NOTE: THIS METHOD IS NOT CURRENTLY USED (keeping in case it is useful in the future)
	// Even though we set a token in our result, the token is not passed into the ProvisioningResult
	// because of how the connectoradapter converts the openconnector Result object
	// The token is only used if the result status is set to "retry".
	
	public Result checkStatus(String id)
			throws ConnectorException {
		logger.debug("Starting checkStatus() with id " + id);
		Result result = new Result();
		if (id != null) {
			String[] splittedId = id.split(TOKEN_DELIMETER);
			String operation = splittedId[0];
			String userId = splittedId[1];
			if (operation.equalsIgnoreCase("Disable")) {
				
				// ok, now that we know we are checking on a disable operation
				// let's load the user and confirm whether or not he/she...
				// 1. is suspended
				// 2. has no groups
				// 3. has no license
				// if any of these things are not true, we will keep the status as queued
				
				// if the user is not found, we will assume that the user has been deleted,
				// and treat that as a success
				
				try {
					logger.debug("Setting up services.");
					setUpServices();
		       
					logger.debug("Retrieving user with id = " + userId);
					User user = getUserById(userId);
					if (user != null && user.isLocked()) {				
						logger.debug("Checking if disable was success for user '" + userId + "' ...");

						boolean committed = true;
						if (user.isSuspended()) {
							logger.info("User with id '" + userId + "' is suspended.");
							result.add("User with id '" + userId + "' is suspended.");
						}
						else {
							// user not suspended
							logger.info("User with id '" + userId + "' is NOT yet suspended.");
							result.add("User with id '" + userId + "' is NOT yet suspended.");
							committed = false;
						}
						
						Group[] groups = userAdminService.getUserGroups(user);
						if (groups != null && groups.length > 0) {
							logger.info("User with id '" + userId + "' still subscribed to one or more groups.");
							result.add("User with id '" + userId + "' still subscribed to one or more groups.");
							committed = false;
						}
						else {
							logger.info("User with id '" + userId + "' has been unsubscribed from all groups.");
							result.add("User with id '" + userId + "' has been unsubscribed from all groups.");
						}
						
						LicenseCategory lc = getLicenseCategory(user.getName());
				        /* If the License Category exists, release */
				        if (lc != null) {
							logger.info("User with id '" + userId + "' still has a license category.");
							result.add("User with id '" + userId + "' still has a license category.");
							committed = false;
						}
				        else {
				        	logger.info("User with id '" + userId + "' has no license.");
				        	result.add("User with id '" + userId + "' has no license.");
				        }
				        
				        // check if everything is committed, and set the result status accordingly
				        if (committed) {
				        	result.setStatus(Result.Status.Committed);
				        }
				        else {
				        	result.setStatus(Result.Status.Queued);
				        	result.setToken(id);
				        }
					}
					else {
						logger.debug("User not found with id = " + userId + ". Assuming the user account was deleted.");
						result.add("User not found with id = " + userId + ". Assuming the user account was deleted.");
						result.setStatus(Result.Status.Committed);
					}

					tearDown();
				} catch (Exception e) {
					if (logger.isErrorEnabled())
						logger.error("Error occurred when checking status of Murex user with ID[ " 
								+ userId + "]" + e.getMessage());
					if (shouldRetry(e)) {
						result.setStatus(Result.Status.Retry);
						result.setToken(id);
						return result;
					}
					tearDown();
					throw new openconnector.ConnectorException(e);
				}
				
			}
			else {
				logger.error("Unsupported operation for checkStatus() - " + operation);
				result.setStatus(Result.Status.Failed);
			}
		}
		else {
			logger.error("Cannot check status of null request token.");
			result.setStatus(Result.Status.Failed);
		}
		logger.debug("Ending checkStatus() with id " + id);
		
		return result;
	}	
	
	private boolean shouldRetry(Exception e) {
		logger.debug("Starting shoultRetry()...");
		boolean ret = false;
		ArrayList<String> _errorList = new ArrayList<String>();
		_errorList
				.add("javax.net.ssl.SSLException: Unrecognized SSL message, plaintext connection?");

		Throwable th = e.getCause();
		if (null == th)
			th = e;

		if (th instanceof java.net.SocketException
				|| th instanceof TimeoutException
				|| th instanceof javax.net.ssl.SSLException) {
			ret = true;
		} else {
			String strActualError = e.getMessage();
			if (!isNullOrEmpty(strActualError)) {
				for (String err : _errorList) {
					if (strActualError.contains(err)) {
						ret = true;
						break;
					}
				}
			}
		}
		logger.debug("Ending shoultRetry().");
		return ret;
	}
	
    /**
     * <p>Returns the user definition for a given user</p>
     *
     * @param  id - the user ID of the User to be retrieved
     *
     * @return - the MX.3 user definition
     *
     * @throws Exception - if there is an error accessing users
     */
    private User getUserById(String id) throws Exception {
        /* Get all the Users in Mx */
        User[] users = userAdminService.getUsers();
        
        int uid;
        // convert id to int
        try {
        	uid = Integer.parseInt(id);
        }
        catch (Exception e) {
        	logger.error("Exception occured while converting id to integer "
					+ e.getMessage());
        	return null;
        }
        /* Iterate over all the users, and if the username is equal to the user we are trying
         * to retrieve we return it */
        for (User user : users) {
            if (uid == user.getUID()) {
                return user;
            }
        }
        /* Throw an Exception if the user is not found */
        throw new Exception("User not found, uid: '" + uid + '\'');
    }
    
    /**
     * <p>Returns the user definition for a given group</p>
     *
     * @param  id - the uid of the Group to be retrieved
     *
     * @return - the MX.3 user definition
     *
     * @throws Exception - if there is an error accessing groups
     */
    private Group getGroupById(String id) throws Exception {
        /* Get all the Users in Mx */
    	// TODO: Fix this API!
        Group[] groups = userAdminService.getGroups(null);
        
        int uid;
        // convert id to int
        try {
        	uid = Integer.parseInt(id);
        }
        catch (Exception e) {
        	logger.error("Exception occured while converting id to integer "
					+ e.getMessage());
        	return null;
        }
        /* Iterate over all the users, and if the username is equal to the user we are trying
         * to retrieve we return it */
        for (Group group : groups) {
            if (uid == group.getUID()) {
                return group;
            }
        }
        /* Throw an Exception if the user is not found */
        throw new Exception("Group not found, uid: '" + uid + '\'');
    }

    /**
     * <p>Returns the LicenseCategory instance that the user is attached to</p>
     *
     * @param  username - the username of the User
     *
     * @return the MX.3 license category. Returns <code>null</code> if the user is not attached to a license
     *
     * @throws Exception if error accessing users
     */
    private LicenseCategory getLicenseCategory(String username) throws Exception {
        /* Get all the License Categories in Mx */
        LicenseCategory[] licenseCategories = licenseAdminService.getLicenseCategories();
        /* Iterate over all the License Categories, and for each License Category iterate over all the Users in its
         * category, and if the username is equal to the user we are trying
         * to retrieve we return that License Category */
        for (LicenseCategory licenseCategory : licenseCategories) {
            User[] users = licenseAdminService.getLicenseCategoryUsers(licenseCategory);
            for (User licenseUser : users) {
                if (username.equals(licenseUser.getName())) {
                    return licenseCategory;
                }
            }
        }
        /* If nothing has been returned, and hence the User does not belong to a license category, return null */
        return null;
    }

    private void tearDown() {
    	logger.debug("Starting tearDown()...");
    	if (userAdminService != null) {
    		userAdminService.cleanUp();
    		userAdminService = null;
    	}
    	if (licenseAdminService != null) {
    		licenseAdminService.cleanUp();
    		licenseAdminService = null;
    	}
    	logger.debug("Ending tearDown()");
    }
   
	/*
	 * Copying some useful stuff from Util class in order to avoid importing as
	 * i am not sure if we can import sailpoint.tools here
	 */
	private boolean isNullOrEmpty(String str) {
		if (str == null) {
			return true;
		}

		return (str.trim().length() == 0);
	}
	
	private boolean isEmpty(Collection collection) {
		return (collection != null) ? collection.isEmpty() : true;
	}

	private boolean isEmpty(Map map) {
		return (map != null) ? map.isEmpty() : true;
	}
}