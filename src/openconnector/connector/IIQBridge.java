
package openconnector.connector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import openconnector.AbstractConnector;
import openconnector.AuthenticationFailedException;
import openconnector.ConnectorConfig;
import openconnector.ConnectorException;
//import openconnector.CopyIterator;
import openconnector.ExpiredPasswordException;
import openconnector.Filter;
import openconnector.FilteredIterator;
import openconnector.Item;
import openconnector.Log;
import openconnector.ObjectAlreadyExistsException;
import openconnector.ObjectNotFoundException;
import openconnector.Result;
import openconnector.Schema;
import openconnector.Connector.Feature;
import openconnector.Item.Operation;
import openconnector.Result.Status;
import openconnector.Schema.Type;
import openconnector.connector.IIQBridge.HTTPHelper;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class IIQBridge extends AbstractConnector {
	public static final String ATTR_ACCT_USERNAME = "userName";
	public static final String ATTR__ACCT_STATUS = "status";
	public static final String ATTR_ACCT_PROXYADRESSES = "proxyAdresses";
	public static final String ATTR_ACCT_DISPLAYNAME = "displayName";
	public static final String ATTR_ACCT_TARGET = "target";
	public static final String ATTR_ACCT_ID = "id";
	public static final String ATTR_ACCT_LOCATION = "location";
	public static final String ATTR_ACCT_CREATED = "created";
	public static final String ATTR_ACCT_VERSION = "version";
	public static final String ATTR_ACCT_LASTMODIFIED = "lastModified";
	public static final String ATTR_ACCT_EMAILS = "emails";
	public static final String ATTR_ACCT_SAMACCOUNTNAME = "samaccountname";
	public static final String ATTR_ACCT_LASTREFRESH = "lastRefresh";
	public static final String ATTR_DISABLED = "disabled";
	public static final String ATTR_LOCKED = "locked";
	public static final String ATTR_PASSWORD = "password";
	
	public static final String ATTR_PASSWORD_OPTIONS = "passwordOptions";
	public static final String ATTR_PASSWORD_HISTORY = "passwordHistory";
	public static final String GROUP_ATTR_NAME = "name";
	public static final String GROUP_ATTR_DESCRIPTION = "description";
	private static String iiqBase = "";
	private static String iiqUser = "";
	private static String iiqPass = "";
	private static final String serviceProviderConfigURL = "ServiceProviderConfig";
	private static Map<String, Map<String, Object>> accounts = new HashMap();
	private static Map<String, Map<String, Object>> groups = new HashMap();
	

	static {
		groups.put(
				"Application.Teradata.Dev.NA_SupplyChain_Display",
				createGroup("Application.Teradata.Dev.NA_SupplyChain_Display",
						""));
		groups.put("Application.HR.Portal.ESS.US",
				createGroup("Application.HR.Portal.ESS.US", ""));
		groups.put(
				"b2s-rg-apps-nikeASP-plng-pmd-leadTimeMinsMgmt",
				createGroup("b2s-rg-apps-nikeASP-plng-pmd-leadTimeMinsMgmt", ""));
		groups.put("Application.HR.Portal.ESS.NL",
				createGroup("Application.HR.Portal.ESS.NL", ""));
		groups.put("Application.COUPA.Prod.Purchasing.User",
				createGroup("Application.COUPA.Prod.Purchasing.User", ""));
		groups.put(
				"Z+APPS_NAE",
				createGroup("Z+APPS_NAE",
						"Application Access - US / EMEA / Americas"));
		groups.put("Z.GD.00000.GB",
				createGroup("Z.GD.00000.GB", "General Display (Global)"));
		groups.put(
				"Z.DL.IB100.GB",
				createGroup("Z.DL.IB100.GB",
						"Maintain Inbound Delivery [Enablers: DL, PL, PD]"));
	}

	public IIQBridge(ConnectorConfig config, Log log) {
		super(config, log);
	}

    public IIQBridge() {
        super();
       // ServiceProviderConfig spc = new ServiceProviderConfig();
        
        System.out.println("Constructor in of IIQBridge Class");
        
        //ArrayList userList = getJsonListObjects(); //method calling that brings the service response and get the list of users
        ArrayList userList = (ArrayList) getServiceResponse(); //method calling that brings the service response and get the list of users
		if(userList != null)
		createAccounts(userList); //creating list of users
		else
			System.out.println("No users from response");
    }

	private void createAccounts(ArrayList userList) {
		System.out.println("In createAccounts() method");
		Iterator iterator = userList.iterator();

		while (iterator.hasNext()) {
			HashMap map = (HashMap) iterator.next();
			String userName = (String) map.get("userName");
			
			String totalResult = (String) map.get("totalResult");
			
			Boolean status = (Boolean) map.get("active");
			String proxyAdresses = (String) map.get("proxyAdresses");
			String id = (String) map.get("id");
			String lastRefresh = (String) map.get("lastRefresh");
			
			String samaccountname = (String) map.get("samaccountname");
			
			String displayName = (String) map.get("displayName");
			
			Map map2 = (Map) map.get("meta");
			Map map3 = (Map) map.get("emails");
			String location = null;
			String created = null;
			String version = null;
			String lastModified = null;
			String emails = null;
			
			if (map2 != null) {
				location = (String) map2.get("location");
				version = (String) map2.get("version");
				created = (String) map2.get("created");
				lastModified = (String) map2.get("lastModified");
			}
			
			if(map3 != null) {
				emails = (String) map3.get("value");
			}
			
			

			//System.out.println("Location nd Version nd created:" + location + " "
			  //		+ version + " " + created + " " +lastModified);
			//System.out.println("In while Display Name :" + displayName);
			//System.out.println("In while Proxyaddress :" + proxyAdresses);
			//System.out.println("In while samaccountname :" + samaccountname);
			
			accounts.put(
					userName,
					createAccount(userName, status.toString(), id, displayName,
							location, version, created, lastModified, lastRefresh, emails, samaccountname, totalResult, new String[] { proxyAdresses }));
		}

	}

	private ArrayList getJsonListObjects(String jsonString) {
		// TODO Auto-generated method stub
		System.out.println("In getJsonListObjects() method");
		//ArrayList list = new ArrayList();

		ArrayList<Map<String, Object>> list = new ArrayList<>();
					
	  //	String jsonString = getServiceResponse();
		
		//String jsonString = null;
		
		if(jsonString != null)
		{

			try{
			JSONObject jsonObj = new JSONObject(jsonString);
			
			//System.out.println(jsonObj.toString());
			JSONArray params = jsonObj.getJSONArray("Resources");
			
			Object usersArray = null;
			JSONObject usersObj = null;
			//System.out.println("1:"+params);
			//System.out.println("2:"+params.length());
			
			int pageIndex = 0;
			Map<String, Object> resourceMap = null;
			Map<String, Object> usersMap = null;
			Map<String, Object> nameMap = null;
			Map<String, Object> emailsMap = null;
			Map<String, Object> metaMap = null;
			Map<String,Object> user2Map = null;
			JSONArray emailsArray = null;
			JSONObject resourcesObj = null;
			JSONObject emailObj = null;
			JSONObject nameObj = null;
			Iterator<String> userIte = null;
			Iterator<String> nameIte = null;
			JSONArray proxyAddArray = null; 
			JSONArray capabilitiesArray = null;
			JSONArray schemasArray = null;
			JSONObject metObj = null;
			Iterator<String> metaIte = null;
			JSONObject user2obj = null;
			Iterator<String> user2Ite = null;
			
			for (int i=0; i< params.length(); i++) {
				//System.out.println("OBJECT:"+(i+1));
				resourcesObj = params.getJSONObject(i);
				resourceMap = new HashMap<String, Object>();
				//resourceMap.put("id",(String) resourcesObj.get("id"));
				//resourceMap.put("id",(String) resourcesObj.get("id"));
				Iterator<String> ite = resourcesObj.keys();
				while(ite.hasNext()){
					String key = ite.next();
					if("id".equalsIgnoreCase(key)){
						resourceMap.put(key, resourcesObj.getString(key));
					}
					else if("urn:ietf:params:scim:schemas:sailpoint:1.0:User".equalsIgnoreCase(key)){
						usersObj = (JSONObject) resourcesObj.get(key);
						//System.out.println(usersObj);
						userIte = usersObj.keys();
						usersMap = new HashMap<>();
						while(userIte.hasNext()){
							String userKey = userIte.next();
							//System.out.println(userKey);
							if("lastRefresh".equalsIgnoreCase(userKey)){
								usersMap.put(userKey, usersObj.getString(userKey));
							}else if("samaccountname".equalsIgnoreCase(userKey)){
								usersMap.put(userKey, usersObj.getString(userKey));
							}else if("isManager".equalsIgnoreCase(userKey)){
								usersMap.put(userKey, usersObj.getBoolean(userKey));
							}
							else if("proxyAdresses".equalsIgnoreCase(userKey)){
								proxyAddArray = usersObj.getJSONArray(userKey);
								if(null != proxyAddArray && proxyAddArray.length() >0){
									
									//System.out.println("if Proxy Address : "+proxyAddArray);
									
									List<JSONArray> list2 = Arrays.asList(proxyAddArray);
									
									//System.out.println("if Proxy Address as List : "+list2);
									//usersMap.put(userKey, list2);
									
									//put the static proxyAddress for testing
									usersMap.put(userKey, "saketh@proxy1.com,saketh2@proxy2.com");
									
								}else{
									//System.out.println("else Proxy Address : "+proxyAddArray);
									usersMap.put(userKey, new JSONArray());
								}
								
							}
						    else if("capabilities".equalsIgnoreCase(userKey)){
						    	capabilitiesArray = usersObj.getJSONArray(userKey);
								if(null != capabilitiesArray && capabilitiesArray.length() >0){
									usersMap.put(userKey, capabilitiesArray.toString());
								}else{
									usersMap.put(userKey, new JSONArray());
								}
							}
							else if("distinguishedName".equalsIgnoreCase(userKey)){
								usersMap.put(userKey, usersObj.getString(userKey));
							}
						}
						//System.out.println("MAAPPPP:"+usersMap.keySet());
						resourceMap.put(key, usersMap);
					}else if("schemas".equalsIgnoreCase(key)){
						schemasArray = resourcesObj.getJSONArray(key);
						if(null != schemasArray && schemasArray.length() >0){
							resourceMap.put(key, schemasArray.toString());
						}else{
							resourceMap.put(key, new JSONArray());
						}
						//resourceMap.put(key, resourcesObj.getString(key));
					}else if("name".equalsIgnoreCase(key)){
						nameObj = (JSONObject) resourcesObj.get(key);
						//System.out.println(usersObj);
						nameIte = nameObj.keys();
						nameMap = new HashMap<>();
						while(nameIte.hasNext()){
							String nameKey = nameIte.next();
							//System.out.println(nameIte.next());
							//nameMap.put("formatted", nameObj.getString("formatted"));
							nameMap.put(nameKey, nameObj.getString(nameKey));
						}
						resourceMap.put(key, nameMap);
					}else if("userName".equalsIgnoreCase(key)){
						resourceMap.put(key, resourcesObj.getString(key));
					}else if("displayName".equalsIgnoreCase(key)){
						//System.out.println("in else if : "+resourcesObj.getString(key));
						resourceMap.put(key, resourcesObj.getString(key));
					}
					else if("active".equalsIgnoreCase(key)){
						resourceMap.put(key, resourcesObj.getBoolean(key));
					}else if("emails".equalsIgnoreCase(key)){
						emailsArray = resourcesObj.getJSONArray(key);
						Iterator<String> emailIte = null;
						
						for (int j=0; j< emailsArray.length(); j++) {
							emailObj = emailsArray.getJSONObject(j);
							emailIte = emailObj.keys();
							emailsMap = new HashMap<>();
							while(emailIte.hasNext()){
								String emailKey = emailIte.next();
								if("primary".equalsIgnoreCase(emailKey)){
									emailsMap.put(emailKey, emailObj.getString(emailKey));
								}else if("value".equalsIgnoreCase(emailKey)){
									emailsMap.put(emailKey, emailObj.getString(emailKey));
								}else if("type".equalsIgnoreCase(emailKey)){
									emailsMap.put(emailKey, emailObj.getString(emailKey));
								}
							}
							
						}
						resourceMap.put(key, emailsMap);
					}
					else if("urn:ietf:params:scim:schemas:extension:enterprise:2.0:User".equalsIgnoreCase(key)){
						user2obj = (JSONObject) resourcesObj.get(key);
						user2Map = new HashMap<>();
						user2Ite = user2obj.keys();
						while(user2Ite.hasNext()){
							String mgrKey = user2Ite.next();
							//No data in json. Handle if needed
							user2Map.put(mgrKey, user2obj.get(mgrKey));
						}
						resourceMap.put(key, user2Map);
					}
					else if("meta".equalsIgnoreCase(key)){
						metObj = resourcesObj.getJSONObject(key);
						metaIte = metObj.keys();
						metaMap = new HashMap<>();
						while(metaIte.hasNext()){
							String metaKey = metaIte.next();
							if("created".equalsIgnoreCase(metaKey)){
								metaMap.put(metaKey, metObj.getString(metaKey));
							}else if("location".equalsIgnoreCase(metaKey)){
								metaMap.put(metaKey, metObj.getString(metaKey));
							}else if("lastModified".equalsIgnoreCase(metaKey)){
								metaMap.put(metaKey, metObj.getString(metaKey));
							}else if("version".equalsIgnoreCase(metaKey)){
								metaMap.put(metaKey, metObj.getString(metaKey));
							}else if("resourceType".equalsIgnoreCase(metaKey)){
								metaMap.put(metaKey, metObj.getString(metaKey));
							}
						}
						resourceMap.put(key, metaMap);
					}
				}
				//System.out.println("resourceMap:"+resourceMap.values().toString());
				list.add(resourceMap);
				pageIndex = pageIndex+1;
			}
			//System.out.println("list:"+list.toString());
			System.out.println("Total Page Index :"+pageIndex);
			System.out.println("Total users list :"+list.size());
			for(Map<String,Object> mapListObj : list){
				//System.out.println("Objects....");
				for(Map.Entry entry : mapListObj.entrySet()){
					//System.out.println(entry.getKey() + ", " + entry.getValue());
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				
		}
		
		return list;
	}

	public ArrayList getServiceResponse() {
		
		iiqUser = "";   /* I hardcoded these values for testing*/
    	iiqPass = "";
		
		ArrayList listOfUsers = new ArrayList();
        //iiqBase = config.getString("host");
    	String iiqBase = "http://nke-lnx-iiq-d008:20809/identityiq/";
    	//iiqBase = this.config.getString("host");
        String listUsers = "scim/v2/Users";
        int startIndex = 1;
        int processedIdentities=0;
        String pageIndex = "";
        int totalResponse=0;
        boolean keepProcessing=true;
        int maxItemsPerPage = 1;
        
        HTTPHelper httpHelper = new HTTPHelper(iiqUser,iiqPass);
		//IIQBridge.HTTPHelper httpHelper = new IIQBridge.HTTPHelper(iiqUser,iiqPass);
				
		CredentialsProvider provider = new BasicCredentialsProvider();
        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(iiqUser, iiqPass);
        provider.setCredentials(AuthScope.ANY, credentials);
        HttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();
					System.out.println("Opened a Connection with WEBSERVICE");

        //String iiqRequest = "https://idlocker-dev.nike.com/identityiq/scim/v2/Users/";
		String iiqRequest = "http://nke-lnx-iiq-d008:20809/identityiq/scim/v2/Users/";
       //System.out.println("\nRequest: " + iiqRequest);
        
        String str=""; 
        HttpGet request = new HttpGet(iiqRequest);
        
        System.out.println("before try block");
        try{
        	
        System.out.println("inside try block");
        HttpResponse response = client.execute(request);
        
        System.out.println("response execusted");
        BufferedReader rd = new BufferedReader (new InputStreamReader(response.getEntity().getContent()));
        String line = "";
                   
      while ((line = rd.readLine()) != null){
        		
        		//System.out.println(line);
        		str = line;
        		
      }
      
     //System.out.println("Json String first time :"+str);
      
      if(str != null)
    	{
    		JSONObject object = new JSONObject(str);
    		
    		//get the totalResponses
    		totalResponse = (int) object.get("totalResults");     		
    		JSONArray params = object.getJSONArray("Resources");
    		
    	}
      	
      System.out.println("Total Responses before while : "+totalResponse);
      System.out.println("maxItemsPerPage before while : "+maxItemsPerPage);
      while(totalResponse > startIndex)
      {
    	  int maxItemsPerThisObject=1;
    	  pageIndex = "?startIndex="+startIndex;
          iiqRequest = iiqBase+listUsers+pageIndex.toString();
          System.out.println("\n\nSCIM 2.0 List Users url:"+iiqRequest);
          String jsonString=httpHelper.httpGet(iiqRequest);
         
          if(jsonString != null)
          {
        	  //System.out.println("in if Jsonstring found:"+jsonString);
        	  JSONObject jsonObject = new JSONObject(jsonString);
        	  
        	  System.out.println("before method call");
        	  ArrayList userList  = getJsonListObjects(jsonString); //calling the method for list of splited json objects
        	  System.out.println("after method call");
        	  
        	  if(userList!=null)
        		  listOfUsers.addAll(userList);
        	  else
        		  System.out.println("user list is empty");
        	  
        	  maxItemsPerThisObject = jsonObject.getJSONArray("Resources").length();
          }
          
    	  maxItemsPerPage = maxItemsPerPage + maxItemsPerThisObject;
    	  startIndex = startIndex+1000;
          
      }
               
        }catch(Exception exception)
        {
        	exception.printStackTrace();
        }
	//return str;
        
        System.out.println(" Total indexes : "+startIndex);
        System.out.println("Total maxPages Size :"+maxItemsPerPage);
        System.out.println("Total Users will be agggregated : "+listOfUsers.size());
        
        return listOfUsers;
	}


	public static Map<String, Object> createAccount(String userName,
			String status, String id, String displayName, String location,
			String version, String created, String lastModified, String lastRefresh, String emails, String samaccountname, String totalResult, String... proxyAdresses) {
		System.out.println("inside the createAccount()");
		System.out.println("inside the createAccount()" + proxyAdresses + " "
				+ proxyAdresses.length);
		HashMap acct = new HashMap();
		acct.put("userName", userName);
		acct.put("status", status);
		acct.put("id", id);
		acct.put("displayName", displayName);
		acct.put("location", location);
		acct.put("version", version);
		acct.put("created", created);
		acct.put("lastModified", lastModified);
		acct.put("lastRefresh", lastRefresh);
		acct.put("emails", emails);
		acct.put("samaccountname", samaccountname);
		ArrayList grpList = null;
		if (proxyAdresses != null) {
			System.out.println("inside proxy if :" + proxyAdresses[0]);
			grpList = new ArrayList(Arrays.asList(proxyAdresses));
		}

		acct.put("proxyAdresses", grpList);
		System.out.println("ProxyAddress : " + grpList);
		return acct;
	}

	public static Map<String, Object> createGroup(String name, String desc) {
		HashMap group = new HashMap();
		group.put("name", name);
		group.put("description", desc);
		return group;
	}

	public static void dump() {
		//System.out.println(accounts);
		//System.out.println(groups);
	}

	public void close() {
	}

	public void testConnection() {
		iiqUser = config.getString("user");
		iiqPass = config.getString("password");
		iiqBase = config.getString("host");
		String iiqRequest = iiqBase + serviceProviderConfigURL;
		String str = "";
		System.out.println("IIQBridge Testing connector: iiqBase = " + iiqBase);
		System.out
				.println("IIQBridge Testing connector: username = " + iiqUser);
		IIQBridge.HTTPHelper httpHelper = new IIQBridge.HTTPHelper(iiqUser,
				iiqPass);
		str = httpHelper.httpGet(iiqRequest);
		JSONObject jsonObject = null;

		try {
			jsonObject = new JSONObject(str);
			String e = jsonObject.get("filter").toString();
			System.out.println("filterValue=" + e);
			JSONObject filterObject = jsonObject.getJSONObject("filter");
			String maxValue = filterObject.get("maxResults").toString();
			System.out.println("maxValue=" + maxValue);
			System.out.println("Connection test successful "
					+ this.getClass().getCanonicalName());
		} catch (JSONException arg7) {
			arg7.printStackTrace();
			System.out.println("Connection test failed");
		}

	}

	public List<Feature> getSupportedFeatures(String objectType) {
		return Arrays.asList(Feature.values());
	}

	public List<String> getSupportedObjectTypes() {
		List types = super.getSupportedObjectTypes();
		types.add("group");
		return types;
	}

	private Map<String, Map<String, Object>> getObjectsMap()
			throws ConnectorException {
		if ("account".equals(this.objectType)) {
			return accounts;
		} else if ("group".equals(this.objectType)) {
			return groups;
		} else {
			throw new ConnectorException("Unhandled object type: "
					+ this.objectType);
		}
	}

	public Result create(String nativeIdentifier, List<Item> items)
			throws ConnectorException, ObjectAlreadyExistsException {
		Result result = new Result(Status.Committed);
		Map existing = this.read(nativeIdentifier);
		if (existing != null) {
			throw new ObjectAlreadyExistsException(nativeIdentifier);
		} else {
			HashMap object = new HashMap();
			object.put(this.getIdentityAttribute(), nativeIdentifier);
			if (items != null) {
				Iterator arg6 = items.iterator();

				while (arg6.hasNext()) {
					Item item = (Item) arg6.next();
					object.put(item.getName(), item.getValue());
				}
			}

			this.getObjectsMap().put(nativeIdentifier, object);
			result.setObject(object);
			return result;
		}
	}

	public Map<String, Object> read(String nativeIdentifier)
			throws ConnectorException, IllegalArgumentException {
		return this.read(nativeIdentifier, false);
	}

	private Map<String, Object> read(String nativeIdentifier, boolean forUpdate)
			throws ConnectorException, IllegalArgumentException {
		if (nativeIdentifier == null) {
			throw new IllegalArgumentException("nativeIdentitifier is required");
		} else {
			Map obj = (Map) this.getObjectsMap().get(nativeIdentifier);
			return forUpdate ? obj : this.copy(obj);
		}
	}

	private Map<String, Object> copy(Map<String, Object> obj) {
		return obj != null ? new HashMap(obj) : null;
	}

	/*public Result update(String nativeIdentifier, List<Item> items)
			throws ConnectorException, ObjectNotFoundException {
		Result result = new Result(Status.Committed);
		Map existing = this.read(nativeIdentifier, true);
		if (existing == null) {
			throw new ObjectNotFoundException(nativeIdentifier);
		} else {
			if (items != null) {
				Iterator arg5 = items.iterator();

				while (arg5.hasNext()) {
					Item item = (Item) arg5.next();
					String name = item.getName();
					Object value = item.getValue();
					Operation op = item.getOperation();
					List currentList;
					List values;
					switch ($SWITCH_TABLE$openconnector$Item$Operation()[op
							.ordinal()]) {
					case 1:
						existing.put(name, value);
						break;
					case 2:
						currentList = getAsList(existing.get(name));
						values = getAsList(value);
						currentList.addAll(values);
						existing.put(name, currentList);
						break;
					case 3:
						currentList = getAsList(existing.get(name));
						values = getAsList(value);
						currentList.removeAll(values);
						if (currentList.isEmpty()) {
							existing.remove(name);
						} else {
							existing.put(name, currentList);
						}
						break;
					default:
						throw new IllegalArgumentException(
								"Unknown operation: " + op);
					}
				}
			}

			return result;
		}
	}*/

	public Result delete(String nativeIdentitifer, Map<String, Object> options)
			throws ConnectorException, ObjectNotFoundException {
		Result result = new Result(Status.Committed);
		Object removed = this.getObjectsMap().remove(nativeIdentitifer);
		if (removed == null) {
			throw new ObjectNotFoundException(nativeIdentitifer);
		} else {
			if (options != null) {
				Iterator keys = options.keySet().iterator();
				if (keys != null) {
					while (keys.hasNext()) {
						String key = (String) keys.next();
						result.add(key + ":" + options.get(key));
					}
				}
			}

			return result;
		}
	}

	public Result enable(String nativeIdentifier, Map<String, Object> options)
			throws ConnectorException, ObjectNotFoundException {
		Result result = new Result(Status.Committed);
		Map obj = this.read(nativeIdentifier, true);
		if (obj == null) {
			throw new ObjectNotFoundException(nativeIdentifier);
		} else {
			obj.put("disabled", Boolean.valueOf(false));
			return result;
		}
	}

	public Result disable(String nativeIdentifier, Map<String, Object> options)
			throws ConnectorException, ObjectNotFoundException {
		Result result = new Result(Status.Committed);
		Map obj = this.read(nativeIdentifier, true);
		if (obj == null) {
			throw new ObjectNotFoundException(nativeIdentifier);
		} else {
			obj.put("disabled", Boolean.valueOf(true));
			return result;
		}
	}

	public Result unlock(String nativeIdentifier, Map<String, Object> options)
			throws ConnectorException, ObjectNotFoundException {
		Result result = new Result(Status.Committed);
		Map obj = this.read(nativeIdentifier, true);
		if (obj == null) {
			throw new ObjectNotFoundException(nativeIdentifier);
		} else {
			obj.put("locked", Boolean.valueOf(false));
			return result;
		}
	}

	public Result setPassword(String nativeIdentifier, String newPassword,
			String currentPassword, Date expiration, Map<String, Object> options)
			throws ConnectorException, ObjectNotFoundException {
		Result result = new Result(Status.Committed);
		Map obj = this.read(nativeIdentifier, true);
		if (obj == null) {
			throw new ObjectNotFoundException(nativeIdentifier);
		} else {
			obj.put("password", newPassword);
			if (expiration != null) {
				if (options == null) {
					options = new HashMap();
				}

				((Map) options).put("expiration", expiration);
			}

			obj.put("passwordOptions", options);
			if (currentPassword != null) {
				Object history = (List) obj.get("passwordHistory");
				if (history == null) {
					history = new ArrayList();
					obj.put("passwordHistory", history);
				}

				((List) history).add(currentPassword);
			}

			return result;
		}
	}

	public Map<String, Object> authenticate(String identity, String password)
			throws ConnectorException, ObjectNotFoundException,
			AuthenticationFailedException, ExpiredPasswordException {
		Map obj = this.read(identity);
		if (obj == null) {
			throw new ObjectNotFoundException(identity);
		} else {
			String actualPassword = (String) obj.get("password");
			if (actualPassword != null && actualPassword.equals(password)) {
				Map passwordsOptions = (Map) obj.get("passwordOptions");
				if (passwordsOptions != null) {
					Date expiration = (Date) passwordsOptions.get("expiration");
					if (expiration != null && expiration.before(new Date())) {
						throw new ExpiredPasswordException(identity);
					}
				}

				return obj;
			} else {
				throw new AuthenticationFailedException();
			}
		}
	}

	public Schema discoverSchema() {
		Schema schema = new Schema();
		if ("account".equals(this.objectType)) {
			schema.addAttribute("userName");
			schema.addAttribute("status");
			schema.addAttribute("proxyAdresses", Type.STRING, true);
			schema.addAttribute("displayName");
			schema.addAttribute("target");
			schema.addAttribute("ID");
			schema.addAttribute("version");
			schema.addAttribute("location");
			schema.addAttribute("created");
			schema.addAttribute("lastModified");
			schema.addAttribute("lastRefresh");
			schema.addAttribute("emails");
			schema.addAttribute("samaccountname");
			
		} else {
			schema.addAttribute("name");
			schema.addAttribute("description");
		}

		return schema;
	}

	public Iterator<Map<String, Object>> iterate(Filter filter)
			throws ConnectorException, UnsupportedOperationException {
		Iterator it = (new ArrayList(this.getObjectsMap().values())).iterator();
		System.out.println("Iterator method end");
		return new CopyIterator(new FilteredIterator(it, filter));
	}
	
	public class CopyIterator
	implements Iterator<Map<String, Object>> {
	    private Iterator<Map<String, Object>> it;

	    public CopyIterator(Iterator<Map<String, Object>> it) {
	        this.it = it;
	        System.out.println("CopyIterator(-) constructor");
	    }

	    @Override
	    public boolean hasNext() {
	        return this.it.hasNext();
	    }

	    @Override
	    public Map<String, Object> next() {
	        return this.copy(this.it.next());
	    }

	    private Map<String, Object> copy(Map<String, Object> obj) {
	        //System.out.println("start of copy()");
	        return obj != null ? new HashMap<String, Object>(obj) : null;
	    }

	    @Override
	    public void remove() {
	        this.it.remove();
	    }
	}

	public class HTTPHelper {
		private final CredentialsProvider provider = new BasicCredentialsProvider();
		private UsernamePasswordCredentials credentials;
		private HttpClient client;

		public HTTPHelper(String iiqUser, String iiqPass) {
			this.credentials = new UsernamePasswordCredentials(iiqUser, iiqPass);
			this.provider.setCredentials(AuthScope.ANY, this.credentials);
			this.client = HttpClientBuilder.create()
					.setDefaultCredentialsProvider(this.provider).build();
		}

		public String httpGet(String url) {
			HttpGet request = new HttpGet(url);
			HttpResponse response = null;
			String jsonStr = "";

			try {
				response = this.client.execute(request);
				BufferedReader e = new BufferedReader(new InputStreamReader(
						response.getEntity().getContent()));

				for (String line = ""; (line = e.readLine()) != null; jsonStr = line) {
					;
				}
			} catch (IOException arg6) {
				arg6.printStackTrace();
			}

			return jsonStr;
		}
	}

	private class IIQBridgeIterator implements Iterator<Map<String, Object>> {
		private Iterator<Map<String, Object>> IIQBridgeIterator;
		Map<String, Object> obj;

		public IIQBridgeIterator(Iterator<Map<String, Object>> arg0) {
			this.IIQBridgeIterator = IIQBridgeIterator;
		}

		public boolean hasNext() {
			return !this.IIQBridgeIterator.hasNext() ? false
					: this.IIQBridgeIterator.hasNext();
		}

		public Map<String, Object> next() {
			this.obj = (Map) this.IIQBridgeIterator.next();
			if (this.obj != null) {
				this.remove();
			}

			return this.obj;
		}

		public void remove() {
			this.IIQBridgeIterator.remove();
		}
	}
}