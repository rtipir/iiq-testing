package openconnector.connector;

/*
import sabrix.UserServiceStub;
import sabrix.UserServiceStub.FindUser;
import sabrix.UserServiceStub.FindUserRequest;
import sabrix.UserServiceCallbackHandler;
*/
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerFactory;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import openconnector.AbstractConnector;
import openconnector.Connector;
import openconnector.ConnectorConfig;
import openconnector.ConnectorException;
import openconnector.Filter;
import openconnector.ObjectNotFoundException;
import openconnector.Result;
import openconnector.Schema.Attribute;
import openconnector.connector.transport.FTPFileTransport;
import openconnector.connector.transport.FileTransport;
import openconnector.connector.transport.SCPFileTransport;

public class SabrixConnector extends AbstractConnector{
	private static org.apache.commons.logging.Log log = LogFactory.getLog(SabrixConnector.class);
	@SuppressWarnings("unused")
	private Iterator<Map<String, Object>> it;
	@SuppressWarnings("unused")
	private static XPathFactory xPathFactory = null;
	XMLFileDownload filedwnld;	
	private static DocumentBuilderFactory domFactory = null;
	 
	 
	 
	 //Parameters for Soap Calls.
	/*private UserServiceStub _sabrixUserServiceStub = null;
	private String _url = null;
	private String _namespaceURI = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	private String _qnamePrefix = "ns";
	private String _passwordType = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText";
	private String _userName = null;
	private String _password = null;
	private int _timeout = 0;
	boolean _stopIteration = false;
	boolean _firstIteration = true; */
	private boolean _bLogEnabled = false;	 
	
	

	public SabrixConnector() throws Exception
	{
		domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true);
		xPathFactory = XPathFactory.newInstance();
		TransformerFactory.newInstance();
		
		this.filedwnld = new XMLFileDownload();
		if (log.isDebugEnabled()) {
		      this._bLogEnabled = true;
		    }
	}
	
	public SabrixConnector(ConnectorConfig config, openconnector.Log logger)
		    throws Exception
		  {
		    super(config, logger);
		    Properties props = System.getProperties();
		    log.debug("Printing the Config Value : " + config.toString());
		    log.debug("Printing the properties Value : " + props.toString());
		    if (log.isDebugEnabled()) {
		      this._bLogEnabled = true;
		    }
		  }
	
    /*public List<Connector.Feature> getSupportedFeatures(String objectType) {
    	System.out.println("Initial from Connector Registry" + Arrays.asList(Feature.values()).toString());
    	List features = new ArrayList();
        features.add(Application.Feature.DIRECT_PERMISSIONS);
        
        features.add(Application.Feature.NO_RANDOM_ACCESS);
        System.out.println("printing features : " + features.toString());
        return features;
        //return Arrays.asList(Feature.values());
    }*/
    
    public List<String> getSupportedObjectTypes() {
        List<String> types = super.getSupportedObjectTypes();
        types.add(OBJECT_TYPE_ACCOUNT);
        return types;
    }
	
	public void testConnection()
			    throws ConnectorException
			  {
				log.debug("Entering the Test Connection");
				String filetransport = this.config.getString("filetransport");
			    String xmlValidation = this.config.getString("xmlValidation");
			    String dataXML = this.config.getString("file");
			    String[] dataXMLArray = null;
			    String attrDefFile = "";
			    int index = 0;
			    
			    log.debug("Test Connection Retrived Values :"+ filetransport + "then XML Validation :"+ xmlValidation + "dataXML file :"+ dataXML);
			    if (dataXML == null)
			    {
			      if (log.isDebugEnabled()) {
			        log.debug("Specify XML Data file.");
			      }
			      throw new ConnectorException("Specify XML Data file.");
			    }
			    if (filetransport == null)
			    {
			      if (log.isDebugEnabled()) {
			        log.debug("Specify File Transport.");
			      }
			      throw new ConnectorException("Specify File Transport.");
			    }
			    if ((filetransport.equals("ftp") == true) || (filetransport.equals("scp") == true))
			    {
			      if ((this.config.getString("host") == null) || (this.config.getString("host").trim().length() == 0))
			      {
			        if (log.isDebugEnabled()) {
			          log.debug("Hostname is blank.");
			        }
			        throw new ConnectorException("Host is blank. Please check.");
			      }
			      if ((this.config.getString("transportUser") == null) || (this.config.getString("transportUser").trim().length() == 0))
			      {
			        if (log.isDebugEnabled()) {
			          log.debug("Username is blank.");
			        }
			        throw new ConnectorException("Username is blank. Please check.");
			      }
			    }
			    attrDefFile = this.config.getString("attrDefinition");
			    if (attrDefFile != null)
			    {
			      if ((filetransport.equals("local") == true) && (!new File(attrDefFile).exists()))
			      {
			    	  log.debug("Test Connection Retrived Values :"+ filetransport + "then XML Validation :"+ xmlValidation + "dataXML file :"+ dataXML + "attributeDef File :"+ attrDefFile);
			        if (log.isDebugEnabled()) {
			          log.debug("File does not exist: " + attrDefFile);
			        }
			        throw new ConnectorException("Test Connection failed. File does not exist: " + attrDefFile);
			      }
			      attrDefFile = attrDefFile.trim();
			    }
			    else
			    {
			      xmlValidation = "false";
			    }
			    dataXMLArray = dataXML.split(";");
			    while (index < dataXMLArray.length) {
			      dataXMLArray[index] = dataXMLArray[(index++)].trim();
			    }
			    index = 0;
			    if (filetransport.equals("local") == true) {
			      while (index < dataXMLArray.length) {
			        if (!new File(dataXMLArray[(index++)]).exists())
			        {
			          if (log.isDebugEnabled()) {
			            log.debug("File does not exist: " + dataXMLArray[(index - 1)]);
			          }
			          throw new ConnectorException("Test Connection failed. File does not exist: " + dataXMLArray[(index - 1)]);
			        }
			      }
			    }
			    try
			    {
			      if (!filetransport.equals("local")) {
			        try
			        {
			          dataXMLArray = downloadFiles(dataXMLArray);
			          if (attrDefFile != null) {
			            attrDefFile = downloadFile(attrDefFile);
			          }
			        }
			        catch (Exception e)
			        {
			          if (log.isDebugEnabled())
			          {
			            log.debug("Test Connection failed. Error: " + e.getMessage());
			            
			          }
			          throw new ConnectorException("Test Connection failed. Error: " + e.getMessage());
			        }
			      }
			    /* boolean attrFileDTD = false;
			      if ((attrDefFile != null) && (attrDefFile.substring(attrDefFile.length() - 3).equals("dtd") == true)) {
			        attrFileDTD = false;
			      } else {
			        attrFileDTD = false;
			      }
			      */
			    }
			    finally
			    {
			      if (!filetransport.equals("local"))
			      {
			        index = 0;
			        while (index < dataXMLArray.length) {
			          new File(dataXMLArray[(index++)]).delete();
			        }
			        if (attrDefFile != null) {
			          new File(attrDefFile).delete();
			        }
			      }
			    }
			    log.debug("Exiting the Test Connection");
			   /* try
			    {
			      checkServerAlive();
			    }
			    catch (Exception e)
			    {
			      if (this._bLogEnabled) {
			        log.debug("Test connection fails. Exception: " + e);
			      }
			      throw new ConnectorException("Test connection fails. Exception: " + e);
			    }*/
			  }

	  public void close()
			    throws ConnectorException
			  {
		  log.debug("Entering the Close Connection");
			    try
			    {
			      /*this._workDayHumanResourceStub.cleanup();
			      this._responseFilter = null;
			      this._workerRequestType = null;
			      if ((null != this._accountsMap) && (this._accountsMap.size() > 0))
			      {
			        this._accountsMap.clear();
			        this._accountsMap = null;
			      }*/
		  log.debug("Exiting the Close Connection");
			    }
			    catch (Exception e)
			    {
			      
			        throw new ConnectorException(e);
			      
			    }
			  }
	  /*
	  public void setObjectType(String type)
	  {
		    if (this._bLogEnabled) {
		        log.debug("Entering function setObjectType ...");
		      }
		    String tempurl = this.config.getString("url");
		    boolean showPassword = false;
		    showPassword = this.config.getBoolean("displayPassword");
		    this._userName = this.config.getString("user");
		    this._password = this.config.getString("password");
		    String custTimeOut = this.config.getString("customTimeOut");
		    if (null != custTimeOut) {
		        this._timeout = Integer.parseInt(custTimeOut);
		      }
		    if (this._bLogEnabled)
		    {
		      log.debug("tempurl= " + tempurl);
		      log.debug("userName= " + this._userName);
		      if (showPassword) {
		        log.debug("password= " + this._password);
		      }
		      log.debug("url=" + this._url);
		      log.debug("timeout= " + this._timeout);
		    }
		    prepareHeaders();
		    if (this._bLogEnabled) {
		        log.debug("Exitting function setObjectType ...");
		      }
	  }
	  
	  public void prepareHeaders()
			    throws ConnectorException
			  {
			    if (this._bLogEnabled) {
			      log.debug("Entering Function prepareHeaders ..................");
			    }
			    try
			    {
			      validateUrl();
			      this._sabrixUserServiceStub = new UserServiceStub(this._url);
			      ServiceClient serviceClient = this._sabrixUserServiceStub._getServiceClient();

			      if (this._timeout != 0)
			      {
			        int customTimeOut = this._timeout * 60 * 1000;
			        serviceClient.getOptions().setTimeOutInMilliSeconds(customTimeOut);
			      }
			      OMFactory omFactory = OMAbstractFactory.getOMFactory();
			      OMElement omSecurityElement = omFactory.createOMElement(new QName(this._namespaceURI, "Security", this._qnamePrefix), null);
			      OMElement omusertoken = omFactory.createOMElement(new QName(this._namespaceURI, "UsernameToken", this._qnamePrefix), null);
			      OMElement omuserName = omFactory.createOMElement(new QName(this._namespaceURI, "Username", this._qnamePrefix), null);
			      omuserName.setText(this._userName);
			      
			      OMElement omPassword = omFactory.createOMElement(new QName(this._namespaceURI, "Password", this._qnamePrefix), null);
			      omPassword.addAttribute("Type", this._passwordType, null);
			      omPassword.setText(this._password);
			      
			      omusertoken.addChild(omuserName);
			      omusertoken.addChild(omPassword);
			      omSecurityElement.addChild(omusertoken);
			      serviceClient.addHeader(omSecurityElement);
			      
			      this._sabrixUserServiceStub._setServiceClient(serviceClient);
			      this._sabrixUserServiceStub._setServiceClient(serviceClient);
			    }
			    catch (Exception e)
			    {			    		      
			        if (this._bLogEnabled) {
			          log.debug("Exception : ", e);
			        }
			        throw new ConnectorException(e);
			    }
			    if (this._bLogEnabled) {
			      log.debug("Exitting prepareHeaders()");
			    }
			  }
	  
	private void validateUrl() throws ConnectorException{
		if (this._bLogEnabled) {
		      log.debug("Entering validateUrl()..... ");
		    }
		    try
		    {
		      URL urlToValidate = new URL(this._url + "?wsdl");
		      HttpURLConnection conn = (HttpURLConnection)urlToValidate.openConnection();
		      int responseCode = conn.getResponseCode();
		      BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
		      String data = null;
		      String inputLine = null;
		      if (((inputLine = in.readLine()) != null) && (null == data)) {
		        data = inputLine;
		      }
		      if (data.indexOf("No service was found.") > -1) {
		        throw new ConnectorException("No service was found. - Verify that service name in URL is spelled correctly");
		      }
		      if (responseCode == 500) {
		        throw new ConnectorException("Verify that service name in URL is spelled correctly");
		      }
		    }
		    catch (Exception e)
		    {
		      throw new ConnectorException(e);
		    }
		    if (this._bLogEnabled) {
		      log.debug("Exitting validateUrl()..... ");
		    }
	}

	private void checkServerAlive() throws ConnectorException{
		
		 if (this._bLogEnabled) {
		      log.debug("Entering checkServerAlive ..................");
		    }
		 try
		    {
			 FindUser findUser = new FindUser();
			 FindUserRequest findUserRequest = new FindUserRequest();
		    /*  Get_Workers_Request workerRequest = new Get_Workers_Request();
		      Get_Workers_RequestType workerRequestType = new Get_Workers_RequestType();
		      Response_FilterType responseFilter = new Response_FilterType();
		      Page_type1 page_type = new Page_type1();
		      Count_type1 count_of_account = new Count_type1();
		      BigDecimal count_per_page = new BigDecimal(1);
		      count_per_page = count_per_page.setScale(0, 6);
		      workerRequestType.setVersion(this._version.trim());
		      count_of_account.setCount_type0(count_per_page);
		      responseFilter.setCount(count_of_account);
		      BigDecimal page_num = new BigDecimal(1);
		      page_num = page_num.setScale(0, 6);
		      page_type.setPage_type0(page_num);
		      responseFilter.setPage(page_type);
		      workerRequestType.setResponse_Filter(responseFilter);
		      
		      workerRequest.setGet_Workers_Request(workerRequestType);
		      if (this._bLogEnabled) {
		        log.debug("Get Worker Request : " + workerRequest.getOMElement(Get_Workers_Request.MY_QNAME, OMAbstractFactory.getOMFactory()));
		      }
		      Get_Workers_Response workerRespone = this._workDayHumanResourceStub.get_Workers(workerRequest);
		      if (this._bLogEnabled) {
		        log.debug("Get Worker Response : " + workerRespone.getOMElement(Get_Workers_Request.MY_QNAME, OMAbstractFactory.getOMFactory()));
		      }
		      //initial coment till above line.
		    }
		    catch (Exception e)
		    {
		          throw new ConnectorException(e);
		    }
		    if (this._bLogEnabled) {
		      log.debug("Exitting checkServerAlive ..................");
		    }
		
	}*/
	
	@Override
	public Iterator<Map<String, Object>> iterate(Filter arg0)
			throws ConnectorException, UnsupportedOperationException {
		
		log.debug("Entering the Iterate Method");
		
	  
	  Document xmlDoc = LoadXML();
	  Iterator<Map<String, Object>> userIterator= readDocfile(xmlDoc);
	  
	  log.debug("Exiting the Iterate Method");
		return userIterator;
	}
	
	  public Map<String, Object> read(String nativeIdentifier)
			    throws ConnectorException, ObjectNotFoundException, UnsupportedOperationException
			  {
		  		log.debug("Entering the read Method");
			    if (this._bLogEnabled) {
			      log.debug("Entering function read...");
			    }
/*			    this._accountsMap = invokeGetWorker("read", nativeIdentifier);
			    Map<String, Object> accPropMap = null;
			    Set<String> keyset = this._accountsMap.keySet();
			    Iterator<String> iter = keyset.iterator();
			    while (iter.hasNext()) {
			      accPropMap = (Map)this._accountsMap.get(iter.next());
			    }*/
			    log.debug("Exiting the read Method");
			    if (this._bLogEnabled) {
			      log.debug("Exitting function read...");
			    }
			    return null;
			  }
	  
	   public Result disable(String nativeIdentifier, Map<String, Object> options)
			    throws ConnectorException, ObjectNotFoundException
			  {
			    Result result = new Result(Result.Status.Committed);
			    return result;
			  }
	  private class SabrixIterator implements Iterator<Map<String, Object>> {
			
			private Iterator<Map<String, Object>> sabrixIterator;
			Map<String, Object> obj;

			public SabrixIterator(Iterator<Map<String, Object>> it) {
				this.sabrixIterator = it;
			}

			public boolean hasNext() {

				if (sabrixIterator.hasNext() == false) {
					return false;
				} else {
					return sabrixIterator.hasNext();
				}
			}

			public Map<String, Object> next() {

				obj = sabrixIterator.next();
				if (obj != null)
					remove();
				return obj;
			}

			public void remove() {
				sabrixIterator.remove();
			}
		}
	  
	  
	 
	  	
	    public Document LoadXML() {
	   	 log.debug("Entering LoadXML method");
	   	 Document xmlDoc = null;
	   	String dataXML = this.config.getString("file");
	   	 
	   	 	   	 
	   	 File xmlFile = new File(dataXML);
	   	 try {
	   	 InputStream fis = new FileInputStream(xmlFile);
	   	 if (fis != null) {
		   	 xmlDoc = getDocFromXMLString(fis);
		   	 //Testing the below to using LIST later iterate over it.
		   	 //List<Map<String, Object>> usermaplist= readDocfile(xmlDoc);
		   	 //log.debug(usermaplist.toString());
		   	 //HashMap<String, String> propertiesKeypair = readPropertyFile();
		   	 log.debug("File Loaded");
		   	
		   	 }
	   	 } catch (FileNotFoundException e) {
		   	 e.printStackTrace();
		   	 } catch (Exception e) {
		   	 e.printStackTrace();
		   	 }
	   	log.debug("Exiting LoadXML method");
	   	return xmlDoc;
	   	 }
	    
		public static Document getDocFromXMLString(InputStream xml)
				 throws Exception {
				 DocumentBuilder builder;
				 Document doc;
				 log.debug("Entering getDocFromXMLString method");
				 try {
				 builder = domFactory.newDocumentBuilder();
				 doc = builder.parse(xml);
				 doc.getDocumentElement().normalize();
				 //log.debug("Root element :"  + doc.getDocumentElement().getNodeName());
				 
				 } catch (Exception exception) {
				 throw exception;
				 } finally {
				 }
				 log.debug("Exiting getDocFromXMLString method");
				 return doc;
		}
	    
		public Iterator<Map<String, Object>> readDocfile(Document doc){
		//public static List<Map<String, Object>> readDocfile(Document doc){
		log.debug("Entering readDocfile method");
		List<Map<String, Object>> sabrixObjectList = new ArrayList<Map<String, Object>>();
		openconnector.Schema schema = null;
		schema = config.getSchema(Connector.OBJECT_TYPE_ACCOUNT);
		doc.getDocumentElement().normalize();
		log.debug("Root element :"  + doc.getDocumentElement().getNodeName());
		NodeList nList = doc.getElementsByTagName("entity");
		 log.debug("----------------------------");
	    for (int temp = 0; temp < nList.getLength(); temp++) {
	       Node nNode = nList.item(temp);
	       //log.debug("\nCurrent Element :" + nNode.getNodeName());
	       if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	          Element eElement = (Element) nNode;
	          //log.debug("name : " + eElement.getAttribute("name"));
	          
	          //Moving to inner Elements.
	          if(eElement.getAttribute("name").equalsIgnoreCase("User")){
	        	  Map<String, Object> sabrixUserMap = userMapConversion(eElement, schema);
	        	  if (!isEmpty(sabrixUserMap))
	        		  sabrixObjectList.add(sabrixUserMap);
	          }         
	       	  }
	    	}
	    log.debug("Exiting readDocfile method");
	    return new SabrixIterator(sabrixObjectList.iterator());
		//return sabrixObjectList;
	    
		 }
		
		public static Map<String, Object> userMapConversion(Element eElement, openconnector.Schema schema){
			//log.debug("Entering userMapConversion method");
			Map<String, Object> _returnMap = new HashMap<String, Object>();
			//TEmporary to see schema
			/*openconnector.Schema schema = new Schema();
			schema.addAttribute("CREATION_DATE");
			schema.addAttribute("DELETED");
			schema.addAttribute("EMAIL");		
			schema.addAttribute("FIRST_NAME");
			schema.addAttribute("LAST_NAME");
			schema.addAttribute("LAST_UPDATE_DATE");		
			schema.addAttribute("START_DATE");
			schema.addAttribute("USERNAME");
			schema.addAttribute("USER_ID");
			schema.addAttribute("END_DATE");*/
			//schema = config.getSchema(Connector.OBJECT_TYPE_ACCOUNT);
							
			List<openconnector.Schema.Attribute> attrs = schema.getAttributes();
			
			if (isEmpty(attrs))
				throw new ConnectorException("Cannot retrieve user schema attributes for Sabrix connector.");
			
			//log.debug("name : " + eElement.getAttribute("name"));
			//log.debug("******STARTING UserMapConversion**********");
			
			try {
				for (openconnector.Schema.Attribute att : attrs) {
					NodeList innerList = eElement.getElementsByTagName("field");
					String attName = att.getName();
					for (int fieldCount = 0; fieldCount < innerList.getLength(); fieldCount++) {
				     	   Node innerNode = innerList.item(fieldCount);
				     	   if (innerNode.getNodeType() == Node.ELEMENT_NODE) {
				                Element innerElement = (Element) innerNode;
				                if (innerElement.getAttribute("name").equalsIgnoreCase(attName)){
				                	Object attVal = null;
				                	attVal = innerElement.getAttribute("value");
				                	_returnMap.put(attName, attVal);
				                	//log.debug("Setting the value of "+ attName + " - "+ innerElement.getAttribute("value"));
				                }
				            }
					}	
				}
			} catch (Exception e) {			
					log.debug("Exception occured while traversing Group list "
							+ e.getMessage());
				
			}
		  //log.debug("*****ENDING UserMapConversion************" + _returnMap.toString());
		  //log.debug(_returnMap.toString());
			//log.debug("Exiting userMapConversion method");
		  return _returnMap;       		
		}
		
		@SuppressWarnings("unused")
		private boolean isNullOrEmpty(String str) {
			if (str == null) {
				return true;
			}

			return (str.trim().length() == 0);
		}
		
		private static boolean isEmpty(Collection<Attribute> collection) {
			return (collection != null) ? collection.isEmpty() : true;
		}

		private static boolean isEmpty(Map<String, Object> map) {
			return (map != null) ? map.isEmpty() : true;
		}
		
	    String[] downloadFiles(String[] sourceFiles)
	    	    throws Exception
	    	  {
	    	    String filetransport = this.config.getString("filetransport");
	    	    int fileCount = sourceFiles.length;
	    	    int index = 0;
	    	    String[] destPath = new String[fileCount];
	    	    while (index < fileCount)
	    	    {
	    	      String filename = new File(sourceFiles[index]).getName();
	    	      destPath[index] = (System.getProperty("java.io.tmpdir") + "/" + index + "_" + System.currentTimeMillis() + filename);
	    	      if (filetransport.equals("ftp") == true) {
	    	        this.filedwnld.ftpFileDownload(log, sourceFiles[index], destPath[index]);
	    	      } else if (filetransport.equals("scp") == true) {
	    	        this.filedwnld.scpFileDownload(log, sourceFiles[index], destPath[index]);
	    	      }
	    	      index++;
	    	    }
	    	    return destPath;
	    	  }
	    
	    String downloadFile(String sourceFile)
	    	    throws Exception
	    	  {
	    	    String destPath = new String();
	    	    String filename = new File(sourceFile).getName();
	    	    destPath = System.getProperty("java.io.tmpdir") + "/" + filename;
	    	    String filetransport = this.config.getString("filetransport");
	    	    if (filetransport.equals("ftp") == true) {
	    	      this.filedwnld.ftpFileDownload(log, sourceFile, destPath);
	    	    } else if (filetransport.equals("scp") == true) {
	    	      this.filedwnld.scpFileDownload(log, sourceFile, destPath);
	    	    }
	    	    return destPath;
	    	  }
	    
	    class XMLFileDownload
	    {
	      XMLFileDownload() {}
	      
	      void ftpFileDownload(org.apache.commons.logging.Log log, String source, String dest)
	        throws Exception
	      {
	        FileTransport ftp = null;
	        String port = "21";
	        ftp = new FTPFileTransport((org.apache.commons.logging.Log) log);
	        Map<String, Object> connectMap = new HashMap<String, Object>();
	        
	        connectMap.put("host", SabrixConnector.this.config.getString("host").trim());
	        connectMap.put("user", SabrixConnector.this.config.getString("transportUser").trim());
	        connectMap.put("password", SabrixConnector.this.config.getString("transportUserPassword"));
	        port = SabrixConnector.this.config.getString("port");
	        if (port == null) {
	          port = "21";
	        }
	        connectMap.put("port", port);
	        try
	        {
	          boolean success = ftp.connect(connectMap);
	          if (!success) {
	            throw new Exception("unable to connect to the ftp server.");
	          }
	          @SuppressWarnings("unused")
			File file = ftp.downloadFile(source, dest);
	          ftp.completeDownload();
	        }
	        finally
	        {
	          if (ftp != null) {
	            ftp.disconnect();
	          }
	        }
	      }
	      
	      void scpFileDownload(org.apache.commons.logging.Log log, String source, String dest)
	        throws Exception
	      {
	        InputStream stream = null;
	        FileTransport scp = null;
	        String port = "22";
	        try
	        {
	          scp = new SCPFileTransport((org.apache.commons.logging.Log) log);
	          
	          Map<String, Object> connectMap = new HashMap<String, Object>();
	          connectMap.put("host", SabrixConnector.this.config.getString("host").trim());
	          connectMap.put("user", SabrixConnector.this.config.getString("transportUser").trim());
	          connectMap.put("password", SabrixConnector.this.config.getString("transportUserPassword"));
	          port = SabrixConnector.this.config.getString("port");
	          if (port == null) {
	            port = "22";
	          }
	          connectMap.put("port", port);
	          boolean success = scp.connect(connectMap);
	          if (!success) {
	            throw new Exception("Unable to connect via scp.");
	          }
	          scp.downloadFile(source, dest);
	          scp.completeDownload();
	        }
	        finally
	        {
	          if (stream != null) {
	            stream.close();
	          }
	          if (scp != null) {
	            scp.disconnect();
	          }
	        }
	      }
	    }


		

}
