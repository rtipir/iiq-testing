package openconnector.connector.HCM;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;

import sailpoint.api.SailPointContext;
import sailpoint.api.SailPointFactory;
import sailpoint.integration.AbstractIntegrationExecutor;
import sailpoint.object.Application;
import sailpoint.object.ProvisioningPlan;
import sailpoint.object.ProvisioningPlan.AccountRequest;
import sailpoint.object.ProvisioningPlan.AccountRequest.Operation;
import sailpoint.object.ProvisioningResult;

public class SAPHCMIntegrationExecutor extends AbstractIntegrationExecutor {
	private static String url;
	private static Map config = new HashMap();
	private static String test_employee;
	private static Application app;
	private static String client_id;
	private static String client_secret;
	private static String authToken;
	private static Log log = LogFactory.getLog(SAPHCMIntegrationExecutor.class);
	private static SailPointContext context;

	public SAPHCMIntegrationExecutor() {
		log.info("Entering the SAPHCMIntegrationExecutor class");
	}

	@Override
	public void configure(Map args) throws Exception {
		if (args != null) {
			config = (Map) args.get("HCMIntegration");
			url = (String) config.get("url");
			client_id = (String) config.get("client_id");
			client_secret = (String) config.get("client_secret");
			test_employee = (String) config.get("test_employee");
		}
		context = SailPointFactory.getCurrentContext();
		// if the current context retrieved through sailPointFactory is null
		// continue with creating one for the scope of the class.
		if (context == null) {
			context = SailPointFactory.createContext();
		}
		client_secret = context.decrypt(client_secret);
		app = context.getObjectByName(Application.class, (String) args.get("app"));

		String authString = client_id + ":" + client_secret;
		byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
		String authStringEnc = new String(authEncBytes);
		// System.out.println("auth Encoded string: " + authStringEnc);
		authToken = "Basic " + authStringEnc;
	}

	public ProvisioningResult provision(ProvisioningPlan plan) {
		ProvisioningResult responseResult = new ProvisioningResult();
		callHCMRestApi api = new callHCMRestApi();
		List<String> errorList = new ArrayList<String>();
		List<AccountRequest> accountRequests = plan.getAccountRequests();
		log.debug("Inside Provisioning Process.");
		int response = 0;
		String responseStr = "";
		try {
			Iterator<AccountRequest> accountRequestsIt = accountRequests.iterator();
			while (accountRequestsIt.hasNext()) {
				AccountRequest accountRequest = accountRequestsIt.next();
				Operation actReqOperation = accountRequest.getOperation();
				List<ProvisioningPlan.AttributeRequest> attributeReqList = accountRequest.getAttributeRequests();
				log.debug("Operation request : " + actReqOperation);
				// log.debug("attributeReqList is "+ attributeReqList);

				if ((actReqOperation.equals(ProvisioningPlan.AccountRequest.Operation.Create))) {
					response = 998;

				} else if ((actReqOperation.equals(ProvisioningPlan.AccountRequest.Operation.Modify))) {
					log.debug(" Starting the Modify Request.");
					response = api.callURL("POST", plan, responseResult, url, authToken, app);

				} else if (actReqOperation.equals(ProvisioningPlan.AccountRequest.Operation.Delete)) {
					response = 998;
				}
			}
			if (response != 0) {
				responseStr = response + "";
			}
			if (response == 201) {
				log.debug(" Provisioning Success - returned 201.");
				responseResult.setStatus(ProvisioningResult.STATUS_COMMITTED);
			} else if (response == 400) {
				log.error(" Provisioning failed - returned 400.");
				responseResult.setStatus(ProvisioningResult.STATUS_FAILED);
				responseResult.addError("Bad Request - Emp Not Active.");
			} else if (errorList != null && errorList.contains(responseStr)) {
				responseResult.setStatus(ProvisioningResult.STATUS_RETRY);
				responseResult.addError("ErrorCode:" + response + " Retrying");
				log.debug("Req Res Retry:" + responseResult.isRetry());
			} else if (response == 999) {
				log.error(" Provisioning failed - Token error - 999.");
				responseResult.setStatus(ProvisioningResult.STATUS_FAILED);
				responseResult.addError("Token Error");
			} else if (response == 998) {
				log.error(" Provisioning failed - Unsupported Operation - 998.");
				responseResult.setStatus(ProvisioningResult.STATUS_FAILED);
				responseResult.addError("Unsupported Operation");
			} else {
				log.error(" Provisioning failed - Error.");
				responseResult.setStatus(ProvisioningResult.STATUS_FAILED);
				responseResult.addWarning("Error is " + response);
			}
		} catch (IOException i) {
			responseResult.setStatus(ProvisioningResult.STATUS_FAILED);
			log.error(i.getMessage());
		} catch (JSONException j) {
			responseResult.setStatus(ProvisioningResult.STATUS_FAILED);
			log.error(j.getMessage());
		} catch (Exception e) {
			responseResult.setStatus(ProvisioningResult.STATUS_FAILED);
			log.error(e.getMessage());
		}
		log.debug(responseResult.toString());
		return responseResult;
	}
}