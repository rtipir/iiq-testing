package openconnector.connector.HCM;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;

import sailpoint.api.IdentityService;
import sailpoint.api.SailPointContext;
import sailpoint.api.SailPointFactory;
import sailpoint.object.Application;
import sailpoint.object.Identity;
import sailpoint.object.Link;
import sailpoint.object.ProvisioningPlan;
import sailpoint.object.ProvisioningPlan.AccountRequest;
import sailpoint.object.ProvisioningResult;
import sailpoint.tools.GeneralException;

public class callHCMRestApi {
	public static String access_token;
	private static Log log = LogFactory.getLog(callHCMRestApi.class);
	private static SailPointContext context;

	public callHCMRestApi() {
		if (log.isInfoEnabled()) {
			log.info("Entering the callHCMRestApi class");
		}
	}

	public int callURL(String method, ProvisioningPlan plan, ProvisioningResult responseResult, String url,
			String authToken, Application app) throws IOException, JSONException, GeneralException {
		int status = 0;
		log.debug("Provisioning plan received : " + plan.toXml());
		List accountrequests = plan.getAccountRequests();
		AccountRequest accountrequest = (AccountRequest) accountrequests.get(0);
		String appNativeid = accountrequest.getNativeIdentity();
		Identity id = plan.getIdentity();
		String samaccountname = (id.getAttribute("samaccountname") != null) ? (String) id.getAttribute("samaccountname")
				: "";
		samaccountname = samaccountname.trim();
		if (samaccountname != null & samaccountname.equals("")) {
			IdentityService idservice = new IdentityService(context);
			List<Link> sapLinks = idservice.getLinks(id, app);
			if (sapLinks != null & sapLinks.size() > 0) {
				Link sapLink = sapLinks.get(0);
				samaccountname = (String) sapLink.getAttribute("SAMACCOUNTNAME");
			}
		}
		String phone = (id.getAttribute("phone") != null) ? (String) id.getAttribute("phone") : "";
		String email = "";
		String mailboxUser = id.getAttribute("isMailboxUser") != null ? (String) id.getAttribute("isMailboxUser") : "";
		if (mailboxUser.equalsIgnoreCase("TRUE")) {
			email = id.getEmail();
		}
		StringBuilder payload = new StringBuilder();
		// payload.append("{\n \"d\": {\n \"__metadata\": {\n \"id\":
		// \"http://pod-dev.nike.com:80/igwj/odata/SAP/ZHRGW_EMP_COMM_DATA_SRV/COMM_DETAILSSet('"+appNativeid+"')\",\n
		// \"uri\":
		// \"http://pod-dev.nike.com:80/igwj/odata/SAP/ZHRGW_EMP_COMM_DATA_SRV/COMM_DETAILSSet('"+appNativeid+"')\",\n
		// \"type\": \"ZHRGW_EMP_COMM_DATA_SRV.COMM_DETAILS\"\n },\n \"EmpId\":
		// \""+appNativeid+"\",\n \"Usrid\": \""+samaccountname+"\",\n
		// \"TelNo\": \""+phone+"\",\n \"EmailId\": \""+email+"\"\n }\n}");
		payload.append("{\n    \"d\": {\n        \"__metadata\": {\n            \"id\": \"" + url + "('" + appNativeid
				+ "')\",\n            \"uri\": \"" + url + "('" + appNativeid
				+ "')\",\n            \"type\": \"ZHRGW_EMP_COMM_DATA_SRV.COMM_DETAILS\"\n        },\n        ");
		payload.append("\"EmpId\": \"" + appNativeid + "\",\n        ");
		payload.append("\"Usrid\": \"" + samaccountname.toUpperCase() + "\",\n        ");
		payload.append("\"TelNo\": \"" + phone + "\",\n        ");
		payload.append("\"EmailId\": \"" + email.toUpperCase() + "\"\n    ");
		payload.append("}\n}");
		log.debug("payload build based on the plan : " + payload);

		try {
			if ("GET".equals(method)) {
				log.debug("Inside GET call");
				status = 998;
			} else if ("POST".equals(method)) {
				log.debug("Inside POST Call URL");
				log.debug("Starting the get request to fetch tokens");

				// create HTTP Client
				HttpClient httpClient = HttpClientBuilder.create().build();
				// Create new getRequest with below mentioned URL
				HttpGet getRequest = new HttpGet(url + "('" + appNativeid + "')?$format=json");
				getRequest.addHeader("authorization", authToken);
				getRequest.addHeader("x-csrf-token", "fetch");
				getRequest.addHeader("Connection", "keep-alive");
				// Execute your request and catch response
				HttpResponse clientResponse = httpClient.execute(getRequest);
				String csrfToken = clientResponse.getFirstHeader("x-csrf-token").getValue();
				log.debug("Succesfully fetched x-csrf-token from get call.");
				if (clientResponse.getStatusLine().getStatusCode() != 200 || null == csrfToken) {
					BufferedReader getBr = new BufferedReader(
							new InputStreamReader((clientResponse.getEntity().getContent())));
					String getOutput;
					while ((getOutput = getBr.readLine()) != null) {
						log.error(" Get request Bad Request or error obtained for " + appNativeid + " " + getOutput);
					}
					status = clientResponse.getStatusLine().getStatusCode();
				} else {
					HttpPost postRequest = new HttpPost(url);
					log.debug("Secured the token and now doing POST call");
					StringEntity jsonData = new StringEntity(payload.toString(), "UTF-8");
					jsonData.setContentEncoding("UTF-8");
					jsonData.setContentType("application/json");
					postRequest.setEntity(jsonData);
					postRequest.setHeader("authorization", authToken);
					postRequest.setHeader("x-csrf-token", csrfToken);
					postRequest.setHeader("Content-Type", "application/json");
					HttpResponse postResponse = httpClient.execute(postRequest);
					log.debug("Response Status Code from POST call - " + postResponse.getStatusLine().getStatusCode());
					status = postResponse.getStatusLine().getStatusCode();
					if (status == 400) {
						BufferedReader postBr = new BufferedReader(
								new InputStreamReader((postResponse.getEntity().getContent())));
						String postOutput;
						while ((postOutput = postBr.readLine()) != null) {
							log.error(" Bad Request or error Obtained during processing " + appNativeid + " "
									+ postOutput);
						}
					}
				}
			} else if ("PUT".equals(method)) {
				log.debug("Inside PUT Call URL");

				status = 998;
			}
		} catch (Exception e) {
			throw new RuntimeException("Exception while calling URL:" + e);
		}
		return status;
	}

	public static int testConnection(String client_id, String client_secret, String url, String appNativeID)
			throws JSONException, ClientProtocolException, IOException, GeneralException {
		log.debug("Testing the connection with Application");
		context = SailPointFactory.getCurrentContext();
		if (context == null) {
			context = SailPointFactory.createContext();
			log.debug("Creating new context.");
		}
		client_secret = context.decrypt(client_secret);

		HttpClient httpClient = HttpClientBuilder.create().build();
		String authString = client_id + ":" + client_secret;
		byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
		String authStringEnc = new String(authEncBytes);
		HttpGet getRequest = new HttpGet(url + "('" + appNativeID + "')?$format=json");
		getRequest.addHeader("authorization", "Basic " + authStringEnc);
		getRequest.addHeader("x-csrf-token", "fetch");
		log.debug("GET message preparation complete.");
		try {
			HttpResponse clientResponse = httpClient.execute(getRequest);
			if (clientResponse.getStatusLine().getStatusCode() != 200) {
				log.debug("Test Connection Failed and failed to get CSRF TOKEN"
						+ clientResponse.getStatusLine().getStatusCode());
				return 999;
			}
			String csrfToken = clientResponse.getFirstHeader("x-csrf-token").getValue();
			log.debug("Test Connection Response Success. CSRF Token -" + csrfToken);
			return 200;
		} catch (Exception e) {
			return 998;
		}
	}

}
