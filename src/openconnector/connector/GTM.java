package openconnector.connector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import openconnector.Result;
import openconnector.Result.Status;
import openconnector.AbstractConnector;
import openconnector.ConnectorConfig;
import openconnector.Connector;
import openconnector.ConnectorException;
import openconnector.Filter;
import openconnector.Item;
import openconnector.FilteredIterator;
import openconnector.ObjectNotFoundException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.io.*;
import javax.net.ssl.HttpsURLConnection;
import java.util.concurrent.TimeoutException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Saketh.Vidala@nike.com
 * 
 *         This connector is used to connect to GTM Portal. 
 *         The first version will aggregate all internal users,
 *         and disable the user.
 * 
 */


public class GTM extends AbstractConnector {

	private static Log logger = LogFactory.getLog(GTM.class);

	public static final String ATTR_ACCT_USERNAME = "userName";
	public static final String ATTR__ACCT_FIRSTNAME = "firstName";
	public static final String ATTR_ACCT_LASTNAME = "lastName";
	public static final String ATTR_ACCT_ROLEGROUP = "roleGroups";
	public static final String ATTR_ACCT_APPLICATIONS = "applications";
	public static final String ATTR_ACCT_ACTIVE = "active";

	public static final String ATTR_GRP_ROLENAME = "roleName";

	private static Map<String, Map<String, Object>> accounts = new HashMap();
	private static Map<String, Map<String, Object>> groups = new HashMap();


	private static final String GTM_HOST = "host";

	public GTM() {

		super();
		logger.debug("GTM Custom Connector Constructor() : ----");
	}


	/*
	 * Returns the type of supported objects(non-Javadoc)
	 * @see openconnector.AbstractConnector#getSupportedObjectTypes()
	 */
	public List<String> getSupportedObjectTypes() {
		List<String> types = super.getSupportedObjectTypes();
		types.add(OBJECT_TYPE_GROUP);
		types.add(OBJECT_TYPE_ACCOUNT);
		return types;
	}

	public GTM(ConnectorConfig config, openconnector.Log log) {
		super(config, log);
		// TODO Auto-generated constructor stub
	}

	private Map<String, Map<String, Object>> getObjectsMap() throws ConnectorException {
		if ("account".equals(this.objectType)) {
			return accounts;
		} else if ("group".equals(this.objectType)) {
			return groups;
		} else {
			throw new ConnectorException("Unhandled object type: " + this.objectType);
		}
	}

	@Override
	public Iterator<Map<String, Object>> iterate(Filter filter)
			throws ConnectorException, UnsupportedOperationException {
		logger.debug("\nFilter in Iterate() : " + filter);

		try{
			if (OBJECT_TYPE_ACCOUNT.equals(getObjectType())) {

				createAccounts();
			}

			else if (OBJECT_TYPE_GROUP.equals(getObjectType())) {

				createGroups();
			}
		} catch(Exception e) {
			logger.debug("Exception occured during " + getObjectType()
					+ " aggregation" + e.getMessage());
			throw new openconnector.ConnectorException(e);
		}

		Iterator it = (new ArrayList(this.getObjectsMap().values())).iterator();

		return new GTMIterator(new FilteredIterator(it, filter));
	}


	public Result delete(String nativeIdentitifer, Map<String, Object> options)
			throws ConnectorException, ObjectNotFoundException {

		logger.debug("Delete");
		Result result = new Result(Status.Committed);
		Object removed = this.getObjectsMap().remove(nativeIdentitifer);
		if (removed == null) {
			throw new ObjectNotFoundException(nativeIdentitifer);
		} else {
			if (options != null) {
				Iterator keys = options.keySet().iterator();
				if (keys != null) {
					while (keys.hasNext()) {
						String key = (String) keys.next();
						result.add(key + ":" + options.get(key));
					}
				}
			}

			return result;
		}
	}


	/*
	 * method that created the accounts for single user we are creating
	 * 
	 */
	public void createAccounts() {

		// call the method to create accounts

		logger.debug("In Create Accounts");
		getUserAccounts();
	}

	public Result disable(String userName, Map<String, Object> options)
			throws ConnectorException, ObjectNotFoundException {

		logger.debug("disabling" + userName);
		return updateSuspendStatus(userName, options, true);
	}

	public Result enable(String userName, Map<String, Object> options)
			throws ConnectorException, ObjectNotFoundException {


		logger.debug("enabling" + userName);
		return updateSuspendStatus(userName, options, false);
	}



	private Map<String, Object> copy(Map<String, Object> obj) {
		
		logger.debug("In copy method");
		return obj != null ? new HashMap(obj) : null;
	}


	private void getUserAccounts() {

		logger.debug("Opened a Connection with WEBSERVICE");
		String iiqBase = config.getString(GTM_HOST);

		int startNo = 1;
		int endNo = 1000;

		while(true)
		{

			String appendURL = "/identity-service/api/users/getAllusers"+"/"+startNo+"/"+endNo+"?fields=[EXTENDED_DETAILS,ACCOUNT,USER_ROLES,ACCOUNT_NAME]";

			String iiqRequest = iiqBase+appendURL;

			logger.debug("iiqRequest" + iiqRequest);

			//System.out.println(startNo+" "+endNo);

			startNo = startNo+1000;

			try {


				URL myurl = new URL(iiqRequest);
				HttpsURLConnection con = (HttpsURLConnection)myurl.openConnection();
				logger.debug("con" + con);

				logger.debug("\nConnection Established");
				InputStream ins = con.getInputStream();
				InputStreamReader isr = new InputStreamReader(ins);
				BufferedReader in = new BufferedReader(isr);       

				String line = "";
				String str="";            
				while ((line = in.readLine()) != null){

					str = line;
				}



				try {

					JSONArray jsonArray = new JSONArray(str);

					logger.debug("jsonArray.length()" + jsonArray.length());


					if(jsonArray.length()==0 || jsonArray==null) 
						break;


					for(int i=0;i<jsonArray.length();i++)
					{

						//System.out.println("i : " + i);

						Map<String, Object> resourceMap = null;
						JSONObject jsonObj = jsonArray.getJSONObject(i);
						JSONObject roleGrpobj = null;
						Iterator<String> roleGrpIte = null;
						JSONArray roleGrpArray = null;
						JSONObject roleObj = null;
						JSONObject roleTypeDescObj = null;
						Iterator<String> ite = jsonObj.keys();
						resourceMap = new HashMap<String, Object>();

						List<openconnector.Schema.Attribute> attrs = schema.getAttributes();
						HashMap<String, Object> acctMap = new HashMap<String, Object>();												


						while (ite.hasNext()) {
							String key = ite.next();
							if ("username".equalsIgnoreCase(key)) {
								resourceMap.put(key, jsonObj.getString(key));
							} else if ("firstName".equalsIgnoreCase(key)) {
								resourceMap.put(key, jsonObj.getString(key));
							} else if ("lastName".equalsIgnoreCase(key)) {
								resourceMap.put(key, jsonObj.getString(key));
							} else if ("active".equalsIgnoreCase(key)) {
								resourceMap.put(key, jsonObj.getBoolean(key));
							} else if ("nikeEmployee".equalsIgnoreCase(key)) {
								resourceMap.put(key, jsonObj.getBoolean(key));
							}else if ("roleGroups".equalsIgnoreCase(key)) {
								roleGrpobj = (JSONObject) jsonObj.get(key);
								roleGrpIte = roleGrpobj.keys();
								ArrayList<String> roleTypeList = new ArrayList<>();
								while (roleGrpIte.hasNext()) {
									String roleGrpKey = roleGrpIte.next();
									roleGrpArray = roleGrpobj.getJSONArray(roleGrpKey);
									for (int j = 0; j < roleGrpArray.length(); j++) {
										roleObj = null;
										roleObj = roleGrpArray.getJSONObject(j);

										String roleDescription = (String) roleObj.get("description");
										roleTypeDescObj = (JSONObject) roleObj.get("roleType");
										// logger.debug(roleTypeDescObj.get("description"));
										String roleTypeDescription = (String) roleTypeDescObj.get("description");

										String roleValue = roleTypeDescription+" ("+roleDescription+")";
										roleTypeList.add(roleValue);
									}
								}
								//resourceMap.put(key, roleTypeList.toString());

								resourceMap.put(key, roleTypeList);
							} else if ("applications".equalsIgnoreCase(key)) {
								JSONArray appArray = jsonObj.getJSONArray(key);
								ArrayList<String> appNamesList = new ArrayList<>();
								for (int k = 0; k < appArray.length(); k++) {
									JSONObject appObj = (JSONObject) appArray.get(k);

									//concatenating the application name and description
									String appName = (String) appObj.get("name");
									String namespace = (String) appObj.get("namespace");			

									String value = appName+" ("+namespace+")";

									appNamesList.add(value);
									//appNamesList.add((String) appObj.get("name"));
								}
								// logger.debug(appArray);
								//resourceMap.put(key, appNamesList.toString());
								resourceMap.put(key, appNamesList);
							}
						}



						// logger.debug("list:"+list.toString());
						for (Map.Entry entry : resourceMap.entrySet()) {
							//logger.debug(entry.getKey() + ", " + entry.getValue());
						}

						if (isEmpty(attrs))
							throw new ConnectorException("Cannot retrieve user schema attributes for GTM connector.");

						if(resourceMap.get("nikeEmployee")!= null && (boolean) resourceMap.get("nikeEmployee")) {
							for (openconnector.Schema.Attribute att : attrs) {
								if (att != null) {
									logger.debug("att : " + att.getName());
									String attName = att.getName();
									Object attVal = null;
									switch (attName) {
									case ATTR_ACCT_USERNAME: 	
										attVal = resourceMap.get("username");
										break;
									case ATTR__ACCT_FIRSTNAME: 
										attVal = resourceMap.get("firstName");
										break;
									case ATTR_ACCT_LASTNAME:
										attVal = resourceMap.get("lastName");
										break;
									case ATTR_ACCT_ROLEGROUP:
										attVal = resourceMap.get("roleGroups");
										break;
									case ATTR_ACCT_APPLICATIONS:
										attVal = resourceMap.get("applications");
										break;
									case ATTR_ACCT_ACTIVE:
										attVal = resourceMap.get("active");
										break;		                
									default:
										logger.debug("Unknown attribute name " + attName);
										break;
									}

									if (attVal != null)
										acctMap.put(attName, attVal);
								}
							}


							if (resourceMap.get("active") != null && (boolean) resourceMap.get("active")) {
								//logger.debug("if clause: ");
								acctMap.put(Connector.ATT_DISABLED, false);
							}
							else {
								//logger.debug("else clause: ");
								acctMap.put(Connector.ATT_DISABLED, true);
							}
							accounts.put(resourceMap.get("username").toString(), acctMap);

						}
					}

				}catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			catch (Exception e) {
				if (logger.isErrorEnabled()) {
					logger.error("Exception occured while traversing User list "
							+ e.getMessage());
				}
			}
		}

	}


	/**
	 * method that creates groups and managed Attributes target to import into
	 * to IIQ through the Group Aggreagtion
	 * 
	 * Parameters : null return : void
	 */
	private void createGroups() {

		logger.debug("In createGroups()");		

		// here need to get the string from URL

		logger.debug("Opened a Connection with WEBSERVICE");

		String iiqBase = config.getString(GTM_HOST);
		String appendURL = "/identity-service/api/identity/roles/all";

		String iiqRequest = iiqBase+appendURL;

		logger.debug("iiqRequest : " + iiqRequest);

		try {
			URL myurl = new URL(iiqRequest);
			HttpsURLConnection con = (HttpsURLConnection)myurl.openConnection();
			logger.debug("con" + con);

			logger.debug("\nConnection Established");
			InputStream ins = con.getInputStream();
			InputStreamReader isr = new InputStreamReader(ins);
			BufferedReader in = new BufferedReader(isr);       

			String line = "";
			String str="";            
			while ((line = in.readLine()) != null){

				str = line;
			}

			try {
				JSONArray rolesArray = new JSONArray(str);

				for (int j = 0; j < rolesArray.length(); j++) {
					Map<String, Object> roleMap = new HashMap<String, Object>();
					JSONObject roleObj = null;
					JSONObject roleTypeObj = null;
					String roleAndTypeDesc = null;
					roleObj = rolesArray.getJSONObject(j);
					if ((roleObj != null && "INTERNAL".equalsIgnoreCase(roleObj.getString("visibilityCode"))) || (roleObj != null && "BOTH".equalsIgnoreCase(roleObj.getString("visibilityCode")))) {
						if (roleObj.get("roleType") != null) {
							roleTypeObj = (JSONObject) roleObj.get("roleType");
							if (roleTypeObj != null) {
								roleAndTypeDesc = roleTypeObj.getString("description") + "(" + roleObj.get("description")
										+ ")";
							}
						}					

						// logger.debug(roleAndTypeDesc);
						// logger.debug("RoleType:"+roleTypeObj);

						roleMap.put(ATTR_GRP_ROLENAME, roleAndTypeDesc);
						groups.put(roleAndTypeDesc, roleMap);
					}
				}
			} catch (Exception exception) {
				exception.printStackTrace();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}

	}

	private Result updateSuspendStatus(String userName, Map<String, Object> options, boolean setSuspend)
			throws ConnectorException, ObjectNotFoundException,
			UnsupportedOperationException {
		logger.debug("Starting updateSuspendStatus()..." + userName);

		Result result = new Result(Result.Status.Committed);
		String operation = "";
		if (setSuspend) {
			logger.debug("Operation is Deactivate");
			operation = "deactivate";
		}
		else {

			logger.debug("Operation is Activate");
			operation = "activate";
		}			

		try {

			if(userName != null) {
				logger.debug("Setting up services.");

				String iiqBase =  config.getString(GTM_HOST);
				String appendURL = "/identity-service/api/users/updateUserStatusByUserName";
				String iiqRequest = iiqBase+appendURL;
				URL myurl = new URL(iiqRequest);
				HttpsURLConnection con = (HttpsURLConnection)myurl.openConnection();
				con.setRequestMethod("POST");
				con.setRequestProperty("username", userName);
				con.setRequestProperty("status", operation);

				logger.debug("con.getResponseCode(); " + con.getResponseCode());

				if(con.getResponseCode() != 200) {

					logger.debug("commit fail");
					//return new Result(Result.Status.Failed);
					result = new Result();
					result.setStatus(Result.Status.Failed);
					String errorMsg = "Exception occurred while Disabling user" + userName + "error code is" + " " + con.getResponseCode();
					logger.debug(errorMsg);
					result.add(errorMsg);
					return result;
				}			

			}

			else {
				logger.debug("User not found with username = " + userName);
				return new Result(Result.Status.Failed);
			}
		} catch (Exception e) {

			logger.debug("in catch block");

			String errormessage = "Error occurred when setting user suspend value" + e.getMessage();

			result = new Result();

			result.add(errormessage);
			if (logger.isErrorEnabled())
				logger.debug("Error occurred when setting user suspend value" + e.getMessage());

			if (shouldRetry(e)) {
				logger.debug("Is retrying");
				return new Result(Result.Status.Retry);
			}


			throw new openconnector.ConnectorException(e);

		}
		logger.debug("Ending updateSuspendStatus() : " + result.toJson());

		return result;
	}


	@Override
	public void testConnection() throws ConnectorException {

		logger.debug("Inside Test Connection");
		String iiqBase = config.getString(GTM_HOST);	
		String appendURL ="/identity-service/api/users/getAllusers/1/7?fields=[ACCOUNT,USER_ROLES,ACCOUNT_NAME]";		
		String iiqRequest = iiqBase+appendURL;

		String str = "";		

		try {
			URL myurl = new URL(iiqRequest);
			HttpsURLConnection con = (HttpsURLConnection)myurl.openConnection();

			if(con.getResponseCode() != 200){
				
				 throw new Exception("Exception occurred" + "error code is" + " " + con.getResponseCode());
			}

			logger.debug("\nConnection Established");
			logger.debug("Connection test successful "
					+ this.getClass().getCanonicalName());
		}catch (Exception e) {

			logger.debug("Connection test failed");
			throw new openconnector.ConnectorException(e);
		}

	}

	public class GTMIterator implements Iterator<Map<String, Object>> {
		private Iterator<Map<String, Object>> it;

		public GTMIterator(Iterator<Map<String, Object>> it) {
			this.it = it;
			logger.debug("GTMIterator(-) constructor");
		}

		@Override
		public boolean hasNext() {
			return this.it.hasNext();
		}

		@Override
		public Map<String, Object> next() {
			return this.copy(this.it.next());
		}

		private Map<String, Object> copy(Map<String, Object> obj) {
			// logger.debug("start of copy()");
			return obj != null ? new HashMap<String, Object>(obj) : null;
		}

		@Override
		public void remove() {
			this.it.remove();
		}
	}

	private boolean shouldRetry(Exception e) {
		logger.debug("Starting shoultRetry()...");
		boolean ret = false;
		ArrayList<String> _errorList = new ArrayList<String>();
		_errorList
		.add("javax.net.ssl.SSLException: Unrecognized SSL message, plaintext connection?");

		Throwable th = e.getCause();
		if (null == th)
			th = e;

		if (th instanceof java.net.SocketException
				|| th instanceof TimeoutException
				|| th instanceof javax.net.ssl.SSLException) {
			ret = true;
		} else {
			String strActualError = e.getMessage();
			if (!isNullOrEmpty(strActualError)) {
				for (String err : _errorList) {
					if (strActualError.contains(err)) {
						ret = true;
						break;
					}
				}
			}
		}
		logger.debug("Ending shoultRetry().");
		return ret;
	}

	private boolean isNullOrEmpty(String str) {
		if (str == null) {
			return true;
		}

		return (str.trim().length() == 0);
	}

	private boolean isEmpty(Collection collection) {
		return (collection != null) ? collection.isEmpty() : true;
	}

	private boolean isEmpty(Map map) {
		return (map != null) ? map.isEmpty() : true;
	}
	
	public Map<String, Object> read(String nativeIdentifier)
			throws ConnectorException, IllegalArgumentException {

		logger.debug("in read method");


		return this.read(nativeIdentifier, false);
	}

	private Map<String, Object> read(String nativeIdentifier, boolean forUpdate)
			throws ConnectorException, IllegalArgumentException {

		logger.debug("in read method 2");
		if (nativeIdentifier == null) {
			throw new IllegalArgumentException("nativeIdentitifier is required");
		} else {
			Map obj = (Map) this.getObjectsMap().get(nativeIdentifier);
			return forUpdate ? obj : this.copy(obj);
		}
	}
	


}