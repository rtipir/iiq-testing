package openconnector.connector;

import java.net.URI;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import openconnector.ConnectorException;
import openconnector.Item;
import openconnector.Result;

import org.json.simple.JSONObject;

/**************************************************************
 * @author Salmath Cheruputhiyakath
 * 
 * This connector extends the OOTB BoxNetConnector to add the functionality 
 * to transfer the ownership of the box account from one user to another.
 * 
 *
 */

public class NikeBoxNetConnector extends BoxNetConnector {

	private static final String ID_ATTR = "id";

	private static final String HTTP_PUT = "PUT";

	private static final String USERS_URI = "/users";
	private static final String SLASH = "/";
	private static final String BOX_20_URL = "https://www.box.com/api/2.0";
	private static final String ALL_FOLDERS = "/folders/0";

	@Override
	public Result update(String objectId, List<Item> items)
			throws ConnectorException
	{       
		Result result = null;
		try
		{
			if(log.isDebugEnabled())
				log.debug("Within update()");

			boolean reassignFlag = false;
			String newOwnerId=null;
			Iterator it = items.iterator();
			while(it.hasNext()){
				Item item = (Item)it.next();
				if(item.getName().equalsIgnoreCase("newOwnerId") && item.getValue() != null){
					log.debug("newOwnerId attribute exists.");
					reassignFlag = true;
					newOwnerId = item.getValue().toString();
					break;
				}
				else if(item.getName().equalsIgnoreCase("disableNewOwner")){
					log.debug("this is a disable request. Removing the disableNewOwner attribute from request");
					it.remove();
					break;
				}
			}

			if(reassignFlag){
				log.debug("reassign request from "+objectId+" to "+newOwnerId);
				result = reassignOwner( objectId, newOwnerId);
			}
			else{
				result = super.update(objectId, items);
			}
		}
		catch(Exception e)
		{
			if(result == null)
				result = new Result();
			result.setStatus(Result.Status.Failed);
			result.add(e.getMessage());
			if (log.isErrorEnabled())
				log.error("Operation failed for object[" + objectId + "]:" + e.getMessage(), e);
		}
		if(log.isDebugEnabled())
			log.debug("Exitted update()");
		return result;
	}

	public Result reassignOwner(String userId, String newOwnerId)
			throws ConnectorException
	{
		Result result = null;

		if (userId == null || newOwnerId == null) {
			result = new Result();
			result.setStatus(Result.Status.Failed);
			String errorMsg = "Exception occurred while reassigning owner. userId and newOwnerId must not be null.";
			log.error(errorMsg);
			result.add(errorMsg);
			return result;
		}

		try
		{   
			if(log.isDebugEnabled())
				log.debug("Within reassignOwner()");

			// Getting both the user and the new owner first... not sure it is necessary
			//Commenting the below lines because they are not necessary 
			//Map userMap = read(userId);  
			//Map owerMap = read(newOwnerId);


			StringBuilder urlStr = new StringBuilder(BOX_20_URL + USERS_URI + SLASH + userId + ALL_FOLDERS);

			if(log.isDebugEnabled())
				log.debug("Retrieving next box page with url: " + urlStr);

			URL url = URI.create(urlStr.toString()).toURL();

			StringBuffer strBuf = new StringBuffer();

			JSONObject newOwnerObj = new JSONObject();
			JSONObject owner = new JSONObject();
			owner.put("id", newOwnerId);
			newOwnerObj.put("owned_by", owner);

			int code = performBoxTransaction(url, HTTP_PUT, newOwnerObj, strBuf);
			result = getBoxResult(code, "reassign ownership of box assets for " + userId, strBuf);

			parseJSONResponse(strBuf, ID_ATTR, null);
			if(strBuf != null){
				if(log.isDebugEnabled())
					log.debug("Server Response:" + strBuf.toString());
			}
		}
		catch(Exception e)
		{
			result = new Result();
			result.setStatus(Result.Status.Failed);
			result.add(e.getMessage());
			log.error("Exception occurred while reassigning owner. Exception Message:" + e.getMessage());
		}
		if(log.isDebugEnabled())
			log.debug("Exited reassignOwner()");

		return result;
	}
}